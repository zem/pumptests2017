# AJ this was to plot three particular wells on top of each other (adjusted water levels so they could be compared more closely). 
# purpose was to examine the signal similarities at that time.

import DataFrames
df = DataFrames
using Compat.Dates
using Base.Dates
import YAML
using PyPlot
plt = PyPlot
import PyCall
@PyCall.pyimport aquiferdb as db
@PyCall.pyimport chipbeta as cb

datemin= DateTime(2017,8,1,0,0,0)
datemax= DateTime(2017,11,15,0,0,0)

yamlfiles = [
"R-11/R-11.yaml"
#"R-35b/R-35b.yaml"
#"R-62/R-62.yaml"
]
dateform = Dates.DateFormat("y-m-d H:M:S") # to convert yaml dates to datetime
mydir=(".")
m=1
mycmap = ["#e6194b", "#000000", "#3cb44b"]
for yamlfile in yamlfiles
        welldir =  split(yamlfile,"/")[1]
        fname = split(yamlfile,"/")[2]
        wellname = split(fname,".")[1]
        println("starting well $wellname")

        yamldata = YAML.load(open(joinpath(mydir,fname)))
	observationwell = yamldata["Observation well"]
	observationbegintime = yamldata["Observation begin time"]
	endtime = yamldata["End time"]

	portdescr=[]
	try
		portdescr = string(yamldata["Screen number"])
	catch
		portdescr = "SINGLE COMPLETION"
	end

	jumpfixdates = []
	jumpfixsize = []
	try
		jumpfixdates = map((x) -> DateTime(x[1],dateform), yamldata["Observation jumps"])
		jumpfixsize = map((x) -> x[2], yamldata["Observation jumps"])
	catch
	end

	baddates = []
	try
		baddates = yamldata["Bad dates"]
	catch
	end

	samplethreshold=[]
	try
		samplethreshold = controldata["Sample threshold"]
	catch
		samplethreshold = 0.08
	end

	samplelength=[]
	try
		samplelength = controldata["Sample length"]
	catch
		samplelength = 5
	end

db.connecttodb()

	# raw water levels
	waterlevels, waterleveltimes = db.getwaterlevels(observationwell, portdescr, observationbegintime, endtime)
	waterlevelsraw=waterlevels;
	waterleveltimesraw=waterleveltimes;
if m==1
	st=find(waterleveltimes -> waterleveltimes > datemin && waterleveltimes < datemax,waterleveltimes)
	mean1=mean(waterlevelsraw[st])
	f, axarray = plt.subplots(2, 1, sharex=true, figsize = (10,15), dpi=300)
#	axarray[1][:plot](waterleveltimesraw, waterlevelsraw, "k.", color=mycmap[m], markersize=0.1)
	axarray[1][:scatter](waterleveltimesraw, waterlevelsraw, 0.2, color=mycmap[m])
else
	st=find(waterleveltimes -> waterleveltimes > datemin && waterleveltimes < datemax,waterleveltimes)
	mean2=mean(waterlevelsraw[st])
	waterlevelsraw=waterlevelsraw+(mean1-mean2)
	axarray[1][:scatter](waterleveltimesraw, waterlevelsraw, 0.2, color=mycmap[m])
end


axarray[1][:set_ylim]([mean1-0.3,mean1+0.3])
axarray[1][:set_title]("Raw Water Levels:")
axarray[1][:set_ylabel]("Water level (m)")
axarray[1][:set_xlim]([datemin,datemax])
#	

	for baddate in baddates
		baddate_datetime = map((x) -> DateTime(x,dateform), hcat(baddate...))
		goodwaterlevels = []
		goodwaterleveltimes = []
		for j in 1:length(waterlevels)
		if waterleveltimes[j] > baddate_datetime[1] && waterleveltimes[j] < baddate_datetime[2]
		else
			push!(goodwaterlevels,waterlevels[j])
			push!(goodwaterleveltimes,waterleveltimes[j])
		end
		end
		waterlevels = goodwaterlevels
		waterleveltimes = goodwaterleveltimes
	end

	j = 1
	for jumpfixdate in jumpfixdates
		i = 1
		while i < length(waterleveltimes) && waterleveltimes[i] < jumpfixdate
		i += 1
		end
		while i < length(waterleveltimes)
		waterlevels[i] += jumpfixsize[j]
		i += 1
		end
		j += 1
	end

barometricbegintime = Dates.format(DateTime(observationbegintime,dateform)+Dates.Day(-1),"yyyy-mm-dd")
	barometricendtime = Dates.format(DateTime(endtime,dateform)+Dates.Day(-1),"yyyy-mm-dd")
	baropressmb, barotimes = db.getbarometricpressure(barometricbegintime, barometricendtime)
earthtideforces, earthtidetimes = db.getearthtide(barometricbegintime, barometricendtime)

waterlevelsnosamp, waterleveltimesnosamp = cb.removeSamplingEvents(waterlevels, waterleveltimes, threshold=samplethreshold, samplinglength=samplelength)
	nosampwaterlevels, nosampwaterleveltimes = cb.denoise(waterlevelsnosamp, waterleveltimesnosamp, baropressmb, barotimes, earthtideforces, earthtidetimes)
	waterlevels = nosampwaterlevels
	waterleveltimes = nosampwaterleveltimes
db.disconnectfromdb()

	# for baddate in baddates
	# 	baddate_datetime = map((x) -> DateTime(x,dateform), hcat(baddate...))
	# 	goodwaterlevels = []
	# 	goodwaterleveltimes = []
	# 	for j in 1:length(waterlevels)
	# 		if waterleveltimes[j] > baddate_datetime[1] && waterleveltimes[j] < baddate_datetime[2]
	# 		else
	# 			push!(goodwaterlevels,waterlevels[j])
	# 			push!(goodwaterleveltimes,waterleveltimes[j])
	# 		end
	# 	end
	# 	waterlevels = goodwaterlevels
	# 	waterleveltimes = goodwaterleveltimes
	# end

if m==1
	st=find(waterleveltimes -> waterleveltimes > datemin && waterleveltimes < datemax,waterleveltimes)
	mean1=mean(waterlevels[st])
#	axarray[2][:plot](waterleveltimes, waterlevels, "k.", color=mycmap[m], markersize=0.1)
	axarray[2][:scatter](waterleveltimes, waterlevels, 0.2, color=mycmap[m])
else
	st=find(waterleveltimes -> waterleveltimes > datemin && waterleveltimes < datemax,waterleveltimes)
	mean2=mean(waterlevels[st])
	waterlevels=waterlevels+(mean1-mean2)
	axarray[2][:scatter](waterleveltimes, waterlevels, 0.2, color=mycmap[m])
end

axarray[2][:set_title]("Processed Water Levels:")
axarray[2][:set_ylabel]("Water level (m)")
axarray[2][:set_xlim]([datemin,datemax])
axarray[2][:set_ylim]([mean1-0.3,mean1+0.3])

majorformatter = matplotlib[:dates][:DateFormatter]("%m/%d")
axarray[1][:xaxis][:set_major_formatter](majorformatter)
	

m=m+1
end

majorformatter = matplotlib[:dates][:DateFormatter]("%m/%d")
axarray[1][:xaxis][:set_major_formatter](majorformatter)

if isdir(joinpath(mydir,"csvfiles"))
else
    mkdir(joinpath(mydir,"csvfiles"))
end 

array=zeros(Float64,length(waterleveltimes),3);
array[:,1]=datetime2julian.(waterleveltimes);
array[:,2]=datetime2julian.(waterleveltimes)-2415018.5;
array[:,3]=waterlevels;
csvfilename=joinpath(mydir,"csvfiles","waterlevels_processed_$(wellname).csv");
writedlm(csvfilename,array,',')
