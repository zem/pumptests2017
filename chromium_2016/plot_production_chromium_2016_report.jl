	# Id: plot_production_chromium_2016_report.jl, Thu 22 Dec 2016 11:04:11 AM MST pandeys #
# Created by Sachin Pandey, LANL
# Description: Plot water levels and rates together FOR REPORTS
#  NOTE: This uses calibration results so that lines are SAME COLORS AS THEIS ANALYSIS
#------------------------------------------------------------------------------
using YAML
using DataFrames
using PyPlot
plt = PyPlot
import PyCall
@PyCall.pyimport aquiferdb as db
@PyCall.pyimport chipbeta as cb
@PyCall.pyimport wellspy as wp
@PyCall.pyimport waterlevelinfo as wl
import DataFrames
df = DataFrames

basedir = pwd()
pumpbegintimes = ["2010-01-01","2016-01-01"]
pumpendtimes = ["2017-01-01","2017-01-01"]

include("wellnames.jl")

if isdir(joinpath(basedir,"figures"))
else
	mkdir(joinpath(basedir,"figures"))
end 

if isdir(joinpath(basedir,"figures","report"))
else
	mkdir(joinpath(basedir,"figures","report"))
end 

for i in 1:length(pumpbegintimes)
	pumpbegintime = pumpbegintimes[i]
	pumpendtime = pumpendtimes[i]

	#------------------------------------------------------------------------------
	# First grab pumping rates
	#------------------------------------------------------------------------------
	# Grab pumping rates for all times
	db.connecttodb()
	production = Dict()
	for productionwell in productionwells
		i = find(productionwells .== productionwell)[1]
		production[productionwell] = Dict()
		production[productionwell]["rates"], production[productionwell]["times"] = db.getpumpingrates(productionwell, pumpbegintime, pumpendtime)
		production[productionwell]["x"], production[productionwell]["y"], production[productionwell]["r"] = db.getgeometry(productionwell)
	end
	db.disconnectfromdb()

	# Get ddnames (needed for plotting colors)
	spoint = wp.SPoint("R-28/R-28.s_point")
	ddnames = spoint[:getdrawdownnames]()
	mycmap = get_cmap("gist_ncar",length(ddnames))

	#------------------------------------------------------------------------------
	# First Plot: Pumping
	#------------------------------------------------------------------------------
	# Initialize figure
	fig, ax = plt.subplots(figsize = (11,5),sharex=true)

	for productionwell in productionwells
		i = find(productionwells .== productionwell)[1]
		i_color = find(ddnames .== productionwell)[1]-2

		results = df.DataFrame(times = production[productionwell]["times"],rates = production[productionwell]["rates"])
		newresults = df.DataFrame(times=DateTime[],rates=Float64[])

		for j in 1:length(results[:,1])-1
			push!(newresults,[results[j,:][:times][1],results[j,:][:rates][1]])
			push!(newresults,[results[j+1,:][:times][1],results[j,:][:rates][1]])
		end
		
		if results[end,:][:rates][1] == 0.0
			push!(newresults,[results[end,:][:times][1],results[end,:][:rates][1]])
		end
		
		if results[1,:][:rates][1] != 0.0
			results = append!(df.DataFrame(times=results[1,:][:times][1],rates=0.0),newresults)
		else
			results = newresults
		end

		ax[:plot](results[:times],results[:rates]*264.172/60/24,label = productionwell,color=mycmap(i_color),linewidth=1.5)
	end

	#ax[:set_xlim](pumpbegintime-10,pumpendtime+10)
	# ax[:set_xlim]([DateTime("$(pumpbegintime)T00:00:00"),DateTime("$(pumpendtime)T00:00:00")])
	ax[:set_ylabel]("Pumping Rate [gpm]")

	labels = ax[:get_xticklabels]() 
	for label in labels
		label[:set_rotation](30) 
	end


	plt.tight_layout()
	box = ax[:get_position]()
	ax[:set_position]([box[:x0], box[:y0], box[:width] * 0.85, box[:height]])

	# sort both labels and handles by labels
	#labels, handles = zip(*sorted(zip(labels, handles), key=lambda t: t[0]))
	handles, labels = ax[:get_legend_handles_labels]()
	labelsandhandles = sort(collect(zip(labels, handles)), by=t->t[1])
	ax[:legend](map(x->x[2], labelsandhandles), map(x->x[1], labelsandhandles),loc=4, bbox_to_anchor=(1.16, 0.0),fontsize=11)

	savefig(joinpath(basedir,"figures","report","pumpingRates_$(pumpbegintime)_$(pumpendtime).png"),dpi=600)

	plt.close()
end
