# Id: plot_waterLevel_rates.jl, Thu 22 Dec 2016 11:04:11 AM MST pandeys #
# Created by Sachin Pandey, LANL
# Description: Plot water levels and rates together
#------------------------------------------------------------------------------
import DataFrames
df = DataFrames
using Compat.Dates
using Base.Dates

include("functions.jl")

if length(ARGS) < 1
    println("Arguments must be of the form mydir")
    println("e.g. julia plot_production_waterlevels.jl chromium_2017")
    quit()
end

mydir = ARGS[1]
include(joinpath(mydir,"wellnames.jl"))

if isdir(joinpath(mydir,"figures"))
else
    mkdir(joinpath(mydir,"figures"))
end 

if isdir(joinpath(mydir,"csvfiles"))
else
    mkdir(joinpath(mydir,"csvfiles"))
end 

if isdir(joinpath(mydir,"figures","production_waterlevels"))
else
    mkdir(joinpath(mydir,"figures","production_waterlevels"))
end 

#		oxys=zeros(Float64,length(yamlfiles),2);
#		j=1;

for yamlfile in yamlfiles
    try
        welldir =  split(yamlfile,"/")[1]
        fname = split(yamlfile,"/")[2]
        wellname = split(fname,".")[1]
#db.connecttodb()		
#oxys[j,1], oxys[j,2], r = db.getgeometry(welldir)
#db.disconnectfromdb()
#j=j+1;
        #------------------------------------------------------------------------------
        # Water Levels
        #------------------------------------------------------------------------------
        waterleveltimes,waterlevels = plotWaterLevels(yamlfile,mydir)

        fig, ax = plt.subplots(figsize = (17,9))
        #ax[:plot](waterleveltimes,waterlevels,"k.")
# AJ added save csv file of water levels
array=zeros(Float64,length(waterleveltimes),2);
array[:,1]=datetime2julian.(waterleveltimes)-2415018.5;
array[:,2]=waterlevels;
csvfilename=joinpath(mydir,"csvfiles","waterlevels_$(wellname).csv");
writedlm(csvfilename,array,',')

		#------------------------------------------------------------------------------
		# Pumping
		#------------------------------------------------------------------------------
		# Don't change these times
		pumpbegintime = "2001-01-01"
		pumpendtime = "2050-01-01"

#		mycmap = get_cmap("Paired",length(productionwells)+1)
#mycmap = get_cmap("tab20",length(productionwells)+1)
mycmap = ["#e6194b", "#000000", "#3cb44b", "#ffe119", "#0082c8", "#f58231", "#911eb4", "#46f0f0", "#f032e6", "#d2f53c", "#fabebe", "#008080", "#e6beff", "#aa6e28", "#800000", "#aaffc3", "#808000", "#ffd8b1", "#000080", "#808080"]
order = [2, 11, 10, 9, 8, 7]
#order = [14, 17, 16, 13, 19, 6, 5, 12, 3, 4, 15, 18]
#order = [2, 11, 10, 9, 8, 7]
#mycmap=[60 180 75 1; 0 128 128 1; 250 190 190 1; 210 245 60 1; 240 50 230 1; 70 240 240 1; 128 0 0 1; 255 215 180 1; 128 128 0 1; 170 110 40 1; 128 128 128 1; 145 30 180 1; 245 130 48 1; 230 190 255 1; 255 225 25 1; 1 130 200 1; 170 255 195 1; 0 0 128 1]

		# Grab pumping rates for all times
		db.connecttodb()
		production = Dict()
		xys=zeros(Float64,length(productionwells),2);
		j=1;
		for productionwell in productionwells
			i = find(productionwells .== productionwell)[1]
			production[productionwell] = Dict()
			production[productionwell]["rates"], production[productionwell]["times"] = db.getpumpingrates(productionwell, pumpbegintime, pumpendtime)
			production[productionwell]["x"], production[productionwell]["y"], production[productionwell]["r"] = db.getgeometry(productionwell)
			xys[j,1]=production[productionwell]["x"];
			xys[j,2]=production[productionwell]["y"];
			if productionwell == "CrIN-6"
				# multiplier is gpm*5.45099404933149
				push!(production["CrIN-6"]["rates"], -109, 0)
				push!(production["CrIN-6"]["times"], DateTime(2017, 11, 8, 10), DateTime(2017, 11, 8, 10, 15))
			# production[productionwell]["x"], production[productionwell]["y"], production[productionwell]["r"] = db.getgeometry(productionwell)
			end
			j=j+1
		end
		db.disconnectfromdb()
#		writedlm("wellocs2.csv", [productionwells xys], ',')
		# Initialize figure
		#ax2 = ax[:twinx]() # Create another axis on top of the current axis

		# Plot data
		for productionwell in productionwells
			i = find(productionwells .== productionwell)[1]
			if length(production[productionwell]["times"]) > 0
				results = df.DataFrame(times = production[productionwell]["times"],rates = production[productionwell]["rates"])
				newresults = df.DataFrame(times=DateTime[],rates=Float64[])

				for j in 1:length(results[:,1])-1
					push!(newresults,[results[j,:][:times][1],results[j,:][:rates][1]])
					push!(newresults,[results[j+1,:][:times][1],results[j,:][:rates][1]])
				end

				if results[end,:][:rates][1] == 0.0
					push!(newresults,[results[end,:][:times][1],results[end,:][:rates][1]])
				end

				if results[1,:][:rates][1] != 0.0
					results = append!(df.DataFrame(times=results[1,:][:times][1],rates=0.0),newresults)
				else
					results = newresults
				end
push!(results,[DateTime(2017,12,31),0])
ax[:plot](results[:times],results[:rates]*264.172/60/24,label = productionwell,color=mycmap[order[i]+1],linewidth=2)
				#ax2[:plot](results[:times],results[:rates]*264.172/60/24,label = productionwell,color=mycmap[i+8],linewidth=1.5)
			end
#array=zeros(Float64,length(results[:times]),2)
#array[:,1]=datetime2julian.(results[:times])-2415018.5;
#array[:,2]=results[:rates]*264.172/60/24;
#csvfilename=joinpath(mydir,"csvfiles","prodwell_$(productionwell).csv")
#writedlm(csvfilename,array,',')
			
		end
		


        labels = ax[:get_xticklabels]()
        for label in labels
            label[:set_rotation](30) 
        end

#        ax[:set_xlim]([minimum(waterleveltimes),maximum(waterleveltimes)])
#ax[:set_xlim]([736584.0,maximum(waterleveltimes)])
#ax[:set_xlim]([736462.5,736466.0])
ax[:set_xlim]([DateTime(2017, 1, 1),DateTime(2017,12,31)])
ax[:tick_params]("both",labelsize=15)
#        ax[:set_xlim]([736630.0,736665.0])

        #ax[:set_title](wellname)
#ax2[:set_ylim]([-100,1200])
#ax[:set_ylim]([1778,1778.5])
        #ax[:set_zorder](ax2[:get_zorder]()+1) # put ax in front of ax2
        ax[:patch][:set_visible](false) # hide the 'canvas'
      #  ax[:set_ylabel]("Water level, (m)")
        #ax[:set_ylabel]("Pumping rate, (m\$^3\$/day)",fontsize=20)
ax[:set_ylabel]("Pumping rate [gpm]",fontsize=20)
        plt.tight_layout()
        # Shrink current axis
        box = ax[:get_position]()
        new_position = [box[:x0], box[:y0], box[:width] * 0.85, box[:height]]
        ax[:set_position](new_position)
       # ax2[:set_position](new_position)

        # Put a legend to the right of the current axis
        ax[:legend](loc="center left", bbox_to_anchor=(1.01, 0.5),fontsize=20)
        

        plt.savefig(joinpath(mydir,"figures","production_waterlevels","production_waterlevels_prodwellsonly.png"),dpi=300)
        ax[:set_xlim]([DateTime(2012, 2, 1),DateTime(2017,12,31)])
        plt.savefig(joinpath(mydir,"figures","production_waterlevels","production_waterlevels_prodwellsonly_allyr.png"),dpi=300)
        plt.close("all")
    catch
        println("problems with $yamlfile")
    end
end
