# Id: plot_theis_driver.py, Thu 05 Jan 2017 03:43:24 PM MST pandeys #
# Created by Sachin Pandey, LANL
# Description: Python script that use wellspy.plotspointfile, aquiferdb, and 
#     chipbeta to plot calibration results against cleaned up data.
#     - Use: Take command line arguments and call plot_theis.py
#------------------------------------------------------------------------------
import sys
import subprocess
import multiprocessing
import time

if len(sys.argv) < 2:
  print "Usage: python plot_theis_driver.py *.yaml"
  sys.exit(1)

pythondir = "./"
filenames = sys.argv[1:]

def dowell(filename):
  # print(filename)
  retval = 0
  retval = subprocess.call("pipenv run python " + pythondir + "plot_theis.py " + filename + " makeplot", shell=True)
  if retval != 0:
    print "error while running plot_theis with " + filename
    return

pool = multiprocessing.Pool(min(50, len(filenames)))
pool.map(dowell, filenames)
