# Id: writeyaml.py, Mon 21 Aug 2017 02:55:26 PM MDT pandeys #
# Created by Sachin Pandey, LANL
# Description: Driver for creating analysis directory, basic YAML files, and wellnames.jl
#------------------------------------------------------------------------------
import yaml
import yamlops
import subprocess
import os
import datetime

def str2bool(v):
	return v.lower() in ("y","yes", "true", "t", "1")

def makeyamlfile(dirname,obswellname):
	fileloc = os.path.join(dirname,obswellname.split("#")[0])
	filename = os.path.join(dirname, obswellname.split("#")[0], obswellname + ".yaml")
	if os.path.isdir(fileloc) == False:
		os.mkdir(fileloc)
	if len(obswellname.split('#'))>1:
		d = {'Observation begin time':' ', 'Pumping begin time':' ', 'Observation well': obswellname.split("#")[0], 'Production wells':' ', 'End time':' ', 'Screen number': int(obswellname.split("#")[1])}
	else:
		d = {'Observation begin time':' ', 'Pumping begin time':' ', 'Observation well': obswellname, 'Production wells':' ', 'End time':' '}
	with open(filename, 'w') as yaml_file:
		yaml.dump(d, yaml_file, default_flow_style=False)

#------------------------------------------------------------------------------
# User data
#------------------------------------------------------------------------------
dirname = "newyamls"
spinuptime = 3.0 # Number of years before observation start that pumping starts

# Dictionary of wells
# obswellnames["wellname"] = ["observation begin time","end time"]
obswellnames = {
"CrIN-6":["2017-01-01","2018-01-01"],
}

productionwells = [
"PM-01",
"PM-02",
"PM-03",
"PM-04",
"PM-05",
"CrEX-1",
"CrEX-2",
"CrEX-3",
"CrEX-4",
"R-28",
"R-42",
"CrIN-1",
"CrIN-2",
"CrIN-3",
"CrIN-4",
"CrIN-5",
"CrIN-6"
]

productionwells_nocounty = [
"CrEX-1",
"CrEX-2",
"CrEX-3",
"CrEX-4",
"R-28",
"R-42",
"CrIN-1",
"CrIN-2",
"CrIN-3",
"CrIN-4",
"CrIN-5",
"CrIN-6"
]

#------------------------------------------------------------------------------
# The code
#------------------------------------------------------------------------------
filenamenodir = map(lambda obswellname: os.path.join(obswellname.split("#")[0], obswellname + ".yaml"),obswellnames.keys())

makethefiles = True
if os.path.isdir(dirname):
	makethefiles = str2bool(raw_input("Warning: Directory already exists, proceed with writing yaml files AND wellnames.jl?\n Note: this will overwrite any yaml files that have been manually customized! "))

if makethefiles:
	if not os.path.isdir(dirname):
		os.mkdir(dirname)

	# Write wellnames.jl
	with open(os.path.join(dirname,"wellnames.jl"),"w") as text_file:
		text_file.write("yamlfiles = [\n")
		for f in filenamenodir:
			text_file.write("\"" + f + "\"\n")
		text_file.write("]\n\n")
		text_file.write("productionwells = [")
		for p in productionwells_nocounty:
			text_file.write("\"" + p + "\", ")
		text_file.write("]")

	# Write yaml files
	for obswellname in obswellnames.keys():
		makeyamlfile(dirname,obswellname)

filenames = subprocess.check_output("ls -1 " + dirname + "/*/*yaml", shell=True).split()
for filename in filenames:
	obswellname = filename.split(".yaml")[0].split("/")[-1]
	cf = yamlops.ControlFile(filename)
	cf.setobservationbegintime(obswellnames[obswellname][0])
	cf.setendtime(obswellnames[obswellname][1])
	cf.setproductionwells(productionwells)
	obsbegintimestr = obswellnames[obswellname][0] 
	test = datetime.datetime.strptime(obswellnames[obswellname][0], '%Y-%m-%d')
	test2 = test - datetime.timedelta(days=spinuptime*365+1)
	cf.setpumpingbegintime(test2.strftime('%Y-%m-%d'))
	cf.dump(filename)

if makethefiles:
	print("Finished writing yaml files and wellnames.jl")
