# Id: findbaddates.jl, Tue 06 Feb 2018 03:26:42 PM MST pandeys #
# Created by Sachin Pandey, LANL
# Description: Use functions.waterLevelBadDates() to find bad dates
# WARNING: THIS WILL OVERWRITE YAMLFILES!!!
#------------------------------------------------------------------------------
import YAML
import PyCall
@PyCall.pyimport yaml

include("functions.jl")

if length(ARGS) < 1
	println("Arguments must be of the form mydir")
	println("e.g. julia findbaddates.jl chromium_2017")
	quit()
end

mydir = ARGS[1]
include(joinpath(mydir,"wellnames.jl"))

# yamlfile = yamlfiles[5]

for yamlfile in yamlfiles
	newbaddates = waterLevelBadDates(joinpath(mydir,yamlfile),timedelta = 1.0)
	if String["2017-06-15 09:00:00", "2017-07-03 09:00:00"] in newbaddates
		println("already in")
	else
		push!(newbaddates,[Dates.format(DateTime(2017,6,15,9),"yyyy-mm-dd HH:MM:SS"),Dates.format(DateTime(2017,7,3,9),"yyyy-mm-dd HH:MM:SS")])
	end
#	push!(newbaddates,[Dates.format(DateTime(2017,7,1),"yyyy-mm-dd HH:MM:SS"),Dates.format(DateTime(2017,7,5),"yyyy-mm-dd HH:MM:SS")])
	yamldata = YAML.load(open(joinpath(mydir,yamlfile))) 
	if length(newbaddates) > 0
		yamldata["Bad dates"] = sort(collect(newbaddates), by=x->x[1])
	end
	run(`rm $(joinpath(mydir,yamlfile))`)
	write(joinpath(mydir,yamlfile), yaml.dump(yamldata, width=255))
end
