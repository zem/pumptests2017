# Pumptests2017: Theis calibration for LANL groundwater

Pumptests2017 is a [ZEM](https://gitlab.com/zem) module.

All modules under ZEM are open-source released under GNU GENERAL PUBLIC LICENSE Version 3.

## Requirements:
- MADS (C version, http://madsc.lanl.gov/)
- Wells (git@gitlab.com:monty/wells.git)
- Python + packages
- Julia + packages

A precompiled version of MADS is found here:
```
/home/vvv/bin/mads16
```

## Description of various python/julia scripts:

### Setup scripts

**writeyaml.py** Write yaml files using yamlops.py
- Creates directories, yaml files, and wellnames.jl
- Note: "User data" in the script must be modified depending on what analysis you are doing
- The yaml files contain the most basic information to do Theis analysis. It does not included well-specific information such as "Bad dates" ranges and "Observation jumps"

**writeyaml_extras.jl** Write additional variables to yaml file based upon yaml files used in previous analysis
- This program copies the yaml variables not written by writeyaml.py ("Bad dates","Observation jumps", "Ignore production wells") from an older Theis analysis and writes them to the new Theis analysis yaml files.

**yamlops.py** Defines class object for writing yaml files

**plot_production.jl** Visualize pumping rates of production wells for a given time range
- Results are in test-directory/figures/production

**plot_production_waterlevels.jl** Visualize water levels (and pumping rates)
- Makes two sets of figures.
  1. test-directory/figures/waterlevels figures show water level data during different stages of processing
  2. test-directory/figures/production_waterlevels show processed water levels and pumping rates
- It is useful to visualize water level to ensure there are no erroneous data.
- Things to look out for:
  1. Gaps in data that are being filled in by the "denoise water levels" algorithm.
  2. Large "jumps" in water level data that could indicate transducer problems (e.g. slip or drift)
	- These can be corrected using "Observation jumps" in the yaml file
	- More importantly, these should be discuss with whoever is providing water level data
  3. Outliers in water level data that occur during sampling events
	- Not to be confused with fast drops that coincide with pump tests (e.g. R-28)
	- These can be removed using "Bad dates" in the yaml file

**findbaddates.jl** Use functions.waterLevelBadDates() to find bad dates.

### Run analysis scripts

** doitall.py ** Runs Theis analysis (in parallel)

Run them all:
```
python doitall.py 1 crex1pumping/*/*.yaml
```
Run one well:
```
python doitall.py 1 crex1pumping/R-28/R-28.yaml
```
**setupmads.py** Called by doitall.py to set up wells and mads files

You can also use setupmads on its own for debugging:
```
python setupmads.py crex1pumping/R-28/R-28.yaml
```

### Forward Runs

**setupmads_nobaddates.py** Called by doitall.py to set up wells and mads files without removing bad dates
- Used for report quality forward runs where model results are continuous in time

**forward_cal.jl** Run calibrated forward model and plot results
- This script uses a modified version of setupmads.py (setupmads_nobaddates.py) so that there are no time gaps in the model output
- Use: visualization of calibration results with continuous model curves

**forward_cal_modpumpstart.jl** Run calibrated forward model and plot results
- This is a modified version of forward_cal.jl that modifies the yaml file  so that all model runs have the same pump begin time
- Use: This is used to compare maximum drawdowns over the same pumping time period. These values are determined using test-directory/maxdd.py and can be used in reports

**forward_cal_waffle.jl** Run calibrated forward model and plot results (no gaps in model run)
- This is a modified version of forward_cal_modpumpstart.jl that modifies files (e.g. \*.yaml, \*.tpl) so that all model runs have the same pump begin time AND the same observation begin time.
- This script also modifies the TPL file so that only one observation is outputted per day over the specified date range
- The model outputs can then be used as observations for waffle model calibration. Observations for waffle calibration are created using test-directory/dd2csv.py
- WARNING: Running this code results in the following type of error, but the code still functions:
```
ERROR: Observation 'P2016-12-30_23_00_00_R-11' is not assigned reading the model output files!
```

**plot_theis.py** This is basically setupmads.py without the call to wellspy.makewellsandmadsfiles, so files used for calibration are not overwritten

**plot_theis_driver.py** Take command line arguments and call plot_theis.py

### Plotting Results in Julia

**plot_cal_forward.jl** Plot results of forward model runs to verify which runs went properly and which need more work

**plot_cal_report.jl** Report style figures

### Scripts specific to analysis directory
There are some non-generic scripts that are located in the analysis "test-directory" (e.g. chromium_2016, rdx_2016 sub-directories)

**test-directory/maxdd.py** Determine maximum drawdown that occurs during model runs (for reports).

**test-directory/dd2csv.py** Write forward_cal_waffle model results to csv file (used as observations for waffle calibration)

**test-directory/wellnames.jl** Includes list of the paths for yaml files and list of production wells. Created by writeyaml.py

**test-directory/plot_params_fieldplots.jl** Use CrPlots to make spatial plot of parameters

**test-directory/plot_production_chromium_2016_report** Report quality figures with colors that match theis colors.

### Other scripts

**functions.jl** Miscellaneous functions. Includes functions for accessing mysql database using aquiferdb.
- plotWaterLevels: Visualize water level data during intermediate preprocessing stages
- waterLevelBadDates: Go through water level data and find bad dates (i.e. large skips in data)
- rawWaterLevels: Get raw (unprocessed) waterlevels from mysql database using aquiferdb
- denoiseWaterLevels: Conduct all stages of processing raw waterlevels and return results
- dbPumpingRates: Get pumpingrates from mysql database using aquiferdb

**calc_rsquared.jl** Calculate r-squared for each Theis analysis.

### RDX Analysis
2016 pumptest analysis of RDX contimated site is in this repo (rdx_2016). The directory contains many files similar to those found in the main directory, but are customized for rdx site.

## MADS commands

Run mads forward simulation
```
mads *.mads forward
```

## Status of using transducer data to estimate county pumping
This is a work in progress. The code to replace crude estimates of county pumping on-off times has not been integrated into the workflow listed above. However, much of the code is written. This code is in the "extras" directory.

## Notes
- If S is large, response to transients smooths out. If K is large, drawdown is negligible
-
