import yaml

class ControlFile:
	def __init__(self, filename):
		with open(filename, "r") as f:
			self.data = yaml.load(f)
			f.close()
	def dump(self, filename):
		with open(filename, "w") as f:
			f.write(yaml.dump(self.data))
			f.close()
	def setendtime(self, endtime):
		self.data["End time"] = endtime
	def setobservationbegintime(self, time):
		self.data["Observation begin time"] = time
	def setproductionwells(self, wellnames):
		self.data["Production wells"] = wellnames
	def setpumpingbegintime(self, time):
		self.data["Pumping begin time"] = time
