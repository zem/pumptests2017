import sys
import subprocess
import multiprocessing
import time

t0 = time.time()

if len(sys.argv) < 3:
	print "Usage: python doitall.py numprocs *.yaml"
	print "numprocs is the number of processors mads will use to do the inverse analysis"
	print "Set numprocs to 1 unless you know what you are doing"
	sys.exit(1)

if len(sys.argv[0].split("/")) > 1:
	pythondir = "/".join(sys.argv[0].split("/")[:-1]) + "/"
else:
	pythondir = "./"
numprocs = sys.argv[1]
filenames = sys.argv[2:]

# additional arguments
debug = True # If debug is true, mads terminal output are not supressed
countypumping = False
countypumpingweights = False

# The code
def dowell(filename):
	retval = 0
	if debug == True:
		retval = subprocess.call("pipenv run python " + pythondir + "setupmads.py " + filename , shell=True)
	else:
		retval = subprocess.call("pipenv run python " + pythondir + "setupmads.py " + filename + " >/dev/null 2>/dev/null", shell=True)
	if retval != 0:
		print "error while running setupmads on " + filename
		return
	rootname = ".".join(filename.split(".")[:-1])
	print(rootname)
#	retval = subprocess.call("fixmads.sh " + rootname + ".mads" , shell=True)
	if debug == True:
		retval = subprocess.call("mads16 " + rootname + ".mads np=" + str(numprocs) , shell=True)
	else:
		retval = subprocess.call("mads16 " + rootname + ".mads np=" + str(numprocs) + " force >/dev/null 2>/dev/null", shell=True)
	if retval != 0:
		print "error while running mads on " + filename
		return
	if debug == True:
		retval = subprocess.call("mads16 " + rootname + "-rerun.mads forward", shell=True)
	else:
		retval = subprocess.call("mads16 " + rootname + "-rerun.mads forward force >/dev/null 2>/dev/null", shell=True)
	if retval != 0:
		print "error while running mads on " + filename
		return
	if debug == True:
		retval = subprocess.call("pipenv run python " + pythondir + "plot_theis.py " + filename + " makeplot", shell=True)
	else:
		retval = subprocess.call("pipenv run python " + pythondir + "plot_theis.py " + filename + " makeplot >/dev/null 2>/dev/null", shell=True)
	if retval != 0:
		print "error while running setupmads with makeplot " + filename
		return

pool = multiprocessing.Pool(min(50, len(filenames)))
#filenames = subprocess.check_output("ls -1 4ippumping/*/*.yaml", shell=True).split("\n")[:-1]
pool.map(dowell, filenames)
#map(dowell, filenames)

t1 = time.time()
total = t1-t0
print(total/60/60)
