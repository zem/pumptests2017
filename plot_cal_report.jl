# Id: plot_results_report.jl, Thu 22 Dec 2016 11:04:11 AM MST pandeys #
# Created by Sachin Pandey, LANL
# Description: Report style figures
#------------------------------------------------------------------------------
using YAML
import DataFrames
df = DataFrames
using PyPlot
plt = PyPlot
import PyCall
@PyCall.pyimport aquiferdb as db
@PyCall.pyimport chipbeta as cb
@PyCall.pyimport wellspy as wp
@PyCall.pyimport waterlevelinfo as wl
using Compat.Dates
using Base.Dates

if length(ARGS) < 1 
	println("Arguments must be of the form mydir")
	println("e.g. julia plot_results_report.jl chromium_2016")
	quit()
end

basedir = pwd()
mydir = ARGS[1]
include(joinpath(basedir,mydir,"wellnames.jl"))

if !isdir(joinpath(basedir,mydir,"figures"))
	mkdir(joinpath(basedir,mydir,"figures"))
end 

if !isdir(joinpath(basedir,mydir,"figures","report"))
	mkdir(joinpath(basedir,mydir,"figures","report"))
end 

for yamlfile in yamlfiles
	@show yamlfile
	yamlindex = find(yamlfiles .== yamlfile)[1]

	# Well name stuff
	welldir = split(yamlfile,"/")[1]
	fname = split(yamlfile,"/")[2]
	wellname = split(fname,".")[1]

	try
		#------------------------------------------------------------------------------
		# First grab pumping rates
		#------------------------------------------------------------------------------
		# Grab pumping rates for all times
		pumpbegintime = "2001-01-01"
		pumpendtime = "2020-01-01"
		db.connecttodb()
		production = Dict()
		for productionwell in productionwells
			i = find(productionwells .== productionwell)[1]
			production[productionwell] = Dict()
			production[productionwell]["rates"], production[productionwell]["times"] = db.getpumpingrates(productionwell, pumpbegintime, pumpendtime)
			production[productionwell]["x"], production[productionwell]["y"], production[productionwell]["r"] = db.getgeometry(productionwell)
			if productionwell == "CrIN-6"
				# multiplier is gpm*5.45099404933149
				push!(production["CrIN-6"]["rates"], -109, 0)
				push!(production["CrIN-6"]["times"], DateTime(2017, 11, 8, 10), DateTime(2017, 11, 8, 10, 15))
			# production[productionwell]["x"], production[productionwell]["y"], production[productionwell]["r"] = db.getgeometry(productionwell)
			end
		end
		db.disconnectfromdb()


		#------------------------------------------------------------------------------
		# Calibration results, total drawdown
		#------------------------------------------------------------------------------
		spoint = wp.SPoint(joinpath(basedir,mydir,"forward",welldir,"$(wellname).s_point"))
		spwellname = spoint[:getwellname]()
		spwaterleveltimes = spoint[:getwaterleveltimes]()
		spwaterlevels = spoint[:getwaterlevels]()
		ddnames = spoint[:getdrawdownnames]()
		drawdowns = spoint[:getdrawdowns]()
		waterlevelinfo = wl.waterlevelinfo(joinpath(basedir,mydir,yamlfile))

		#------------------------------------------------------------------------------
		# Plot Observation dataRaw data
		#------------------------------------------------------------------------------
		# Initialize figure
		fig, ax = plt.subplots(2,figsize=(9.0,8.0))
		#mycmap = get_cmap("gist_ncar",length(ddnames))
		mycmap = ["#e6194b", "#000000", "#3cb44b", "#ffe119", "#0082c8", "#f58231", "#911eb4", "#46f0f0", "#f032e6", "#d2f53c", "#fabebe", "#008080", "#e6beff", "#aa6e28", "#800000", "#aaffc3", "#808000", "#ffd8b1", "#000080", "#808080"]
		order = [14, 17, 16, 13, 19, 6, 5, 12, 3, 4, 15, 18]

		# mycmap = get_cmap("Paired",length(ddnames)-1)

		# YAML stuff
		yamldata = YAML.load(open(joinpath(basedir,mydir,"forward",welldir,fname)))
		observationwell = yamldata["Observation well"]

		portdescr=[]
		try
			portdescr = string(yamldata["Screen number"])
		catch
			portdescr = "SINGLE COMPLETION"
		end

		ax[1][:plot](waterlevelinfo[spwellname]["times"], waterlevelinfo[spwellname]["waterlevels"], ls=" ", color = "0.45", marker = ".")
		ax[1][:plot](spwaterleveltimes, spwaterlevels, "k", linewidth=2, label="total")

		if portdescr == "SINGLE COMPLETION"
			ax[1][:set_title]("$(observationwell)\n(A)")
		else
			ax[1][:set_title]("$(observationwell)#$(portdescr)\n(A)")
		end

		#------------------------------------------------------------------------------
		# Plot Pumping rates
		#------------------------------------------------------------------------------
		ax2 = ax[1][:twinx]() # Create another axis on top of the current axis
		for productionwell in productionwells
			i = find(productionwells .== productionwell)[1]
			i_color = find(ddnames .== productionwell)[1]-2

			results = df.DataFrame(times = production[productionwell]["times"],rates = production[productionwell]["rates"])
			newresults = df.DataFrame(times=DateTime[],rates=Float64[])
		
			for j in 1:length(results[:,1])-1
				push!(newresults,[results[j,:][:times][1],results[j,:][:rates][1]])
				push!(newresults,[results[j+1,:][:times][1],results[j,:][:rates][1]])
			end

			if results[end,:][:rates][1] == 0.0
				push!(newresults,[results[end,:][:times][1],results[end,:][:rates][1]])
			end

			if results[1,:][:rates][1] != 0.0
				results = append!(df.DataFrame(times=results[1,:][:times][1],rates=0.0),newresults)
			else
				results = newresults
			end

			ax2[:plot](results[:times],results[:rates]*264.172/60/24,label = productionwell,color=mycmap(i_color),linewidth=1.5)
			# ax2[:bar](df[:times],df[:rates]*264.172/60/24,label = productionwell,color=mycmap(i_color),align="center",edgecolor=mycmap(i))
		end

		ax[1][:set_xlim](minimum(spwaterleveltimes),maximum(spwaterleveltimes))
		ax[1][:set_zorder](ax2[:get_zorder]()+1) # put ax in front of ax2 
		ax[1][:patch][:set_visible](false) # hide the 'canvas' 

		#------------------------------------------------------------------------------
		# Calibration results, individual drawdown
		#------------------------------------------------------------------------------

		ddnames_labels = map((ddname) -> replace(ddname,"0",""), ddnames) # remove 0 from well names
		mod_dd = [] # used to recalculate total

		for i in 1:length(ddnames)
			mindd = minimum(drawdowns[ddnames[i]])
			if ddnames[i] == "trend"
				ax[2][:plot](spwaterleveltimes, map((dd) -> dd - mindd, drawdowns[ddnames[i]]), label = "trend", c="k", ls="--",linewidth=1.5)
				# append!(mod_dd, (map((dd) -> dd - mindd, drawdowns[ddnames[i]])))
				mod_dd = (map((dd) -> dd - mindd, drawdowns[ddnames[i]]))
			end
		end

		# Then plot wells
		ctr_col = 1 # iterator for custom colormap
		for i in 1:length(ddnames)
			mindd = minimum(drawdowns[ddnames[i]])
			if ddnames[i] != "total" && ddnames[i] != "trend"
				# append!(mod_dd, (map((dd) -> dd - mindd, drawdowns[ddnames[i]])))
				mod_dd = [mod_dd (map((dd) -> dd - mindd, drawdowns[ddnames[i]]))]
				ax[2][:plot](spwaterleveltimes, map((dd) -> dd - mindd, drawdowns[ddnames[i]]), c=mycmap(ctr_col), label = ddnames_labels[i],linewidth=1.5)
				ctr_col += 1
			end
		end

		# Then plot total
		# PROBLEM: Sometimes individual drawdowns are greater than total!
		# SOLUTION 1: calculate a new total, which is done in the for loop above (mod_dd) 
		total_dd = []
		for i in 1:length(mod_dd[:,1])
			append!(total_dd,sum(mod_dd[i,:]))
		end
		ax[2][:set_xlim]([minimum(spwaterleveltimes),maximum(spwaterleveltimes)])
		ax[2][:plot](spwaterleveltimes, total_dd, label = "total", c="k", linewidth=2)
		ax[2][:invert_yaxis]()
		ax[2][:set_title]("(B)")

		#------------------------------------------------------------------------------
		# Make plots pretty
		#------------------------------------------------------------------------------
		labels = ax[1][:get_xticklabels]() 
		for label in labels
			label[:set_rotation](20) 
		end

		labels = ax[2][:get_xticklabels]() 
		for label in labels
			label[:set_rotation](20) 
		end

		ax[1][:set_ylabel]("Water level (m)")
		ax2[:set_ylabel]("Pumping rate, (gpm)")
		ax[2][:set_ylabel]("Drawdown (m)")
		#ax[2][:set_xlabel]("Date")

		#ax[1][:set_xlim](DateTime("2016-01-01T00:00:00"),DateTime("2017-01-01T00:00:00"))
		#ax[2][:set_xlim](DateTime("2016-01-01T00:00:00"),DateTime("2017-01-01T00:00:00"))

		#ax[1][:set_xlim](DateTime("2016-05-01T00:00:00"),DateTime("2017-01-01T00:00:00"))
		#ax[2][:set_xlim](DateTime("2016-05-01T00:00:00"),DateTime("2017-01-01T00:00:00"))

		plt.tight_layout(pad=1.0,w_pad=1.0,h_pad=1.0)
		
		box = ax[1][:get_position]()
		ax[1][:set_position]([box[:x0], box[:y0], box[:width] * 0.85, box[:height]])

		box = ax2[:get_position]()
		ax2[:set_position]([box[:x0], box[:y0], box[:width] * 0.85, box[:height]])

		box = ax[2][:get_position]()
		ax[2][:set_position]([box[:x0], box[:y0], box[:width] * 0.85, box[:height]])

		handles, labels = ax[2][:get_legend_handles_labels]()
		# sort both labels and handles by labels
		labelsandhandles = sort(collect(zip(labels, handles)), by=t->t[1])
		ax[2][:legend](map(x->x[2], labelsandhandles), map(x->x[1], labelsandhandles),loc=4, bbox_to_anchor=(1.28, 0.0),fontsize=11)

		# sort both labels and handles by labels
		#labels, handles = zip(*sorted(zip(labels, handles), key=lambda t: t[0]))
		handles, labels = ax2[:get_legend_handles_labels]()
		handles2, labels2 = ax[1][:get_legend_handles_labels]() 
		append!(handles,handles2)
		append!(labels,labels2)
		labelsandhandles = sort(collect(zip(labels, handles)), by=t->t[1])
		ax2[:legend](map(x->x[2], labelsandhandles), map(x->x[1], labelsandhandles),loc=1, bbox_to_anchor=(1.28, 1.0),fontsize=11)

		plt.savefig(joinpath(basedir,mydir,"figures","report","$(wellname).png"),dpi=600)
		# plt.savefig(joinpath(basedir,mydir,"figures","report","$(wellname).png"))
		plt.close()
	catch
		println("problems with $yamlfile")
 	end
end
