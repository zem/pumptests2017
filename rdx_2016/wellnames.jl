yamlfiles = [
            "CdV-9-1i/CdV-9-1i#1.yaml",
            "CdV-9-1i/CdV-9-1i-Pz-1.yaml",
            "CdV-9-1i/CdV-9-1i-Pz-2.yaml",
            "CdV-16-1i/CdV-16-1i.yaml",
            "CdV-16-2ir/CdV-16-2ir.yaml",
            "CdV-16-4ip/CdV-16-4ip#1.yaml",
            "R-25/R-25#1.yaml",
            "R-25/R-25#2.yaml",
            "R-25/R-25#4.yaml",
            "R-25/R-25#4-2014.yaml",            # this is for a 2014 pump test (different forward_samestart time)
            "R-25/R-25#4-2014-allWells.yaml",   # this is for a 2014 pump test (different forward_samestart time)
            "R-25b/R-25b.yaml",
            "R-47i/R-47i.yaml",
            "R-63/R-63.yaml",
            "R-63i/R-63i.yaml",
            ]   

productionwells = ["CdV-16-4ip","CdV-16-1(i)","CdV-9-1(i)"]

