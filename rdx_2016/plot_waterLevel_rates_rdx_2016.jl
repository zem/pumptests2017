# Id: plot_waterLevel_rates.jl, Thu 22 Dec 2016 11:04:11 AM MST pandeys #
# Created by Sachin Pandey, LANL
# Description: Plot water levels and rates together
#------------------------------------------------------------------------------
using YAML
using DataFrames
using PyPlot
plt = PyPlot
import PyCall
@PyCall.pyimport aquiferdb as db
@PyCall.pyimport chipbeta as cb
@PyCall.pyimport wellspy as wp
@PyCall.pyimport waterlevelinfo as wl
@PyCall.pyimport matplotlib.ticker as mtick

mydir = "rdx_2016_report"
# basedir = "/lclscratch/sach/pumptests2016"
basedir = "/scratch/ymp/sach/Working/pumptests/pumptests2016"

yamlfiles = [ 
            "CdV-16-4ip/CdV-16-4ip#1.yaml",
            "CdV-16-1i/CdV-16-1i.yaml",
            "CdV-9-1i/CdV-9-1i#1.yaml",
            ]

customlabels = ["CdV-16-4ip","CdV-16-1i","CdV-9-1i"]
               

wellinfo = ["CdV-16-4ip-S1","SINGLE COMPLETION"],
           ["CDV-16-1(i)","SINGLE COMPLETION"], # no data during tracer injection
           ["CDV-9-1(i)","1"]

datelimits = [
[DateTime("2016-08-19T00:00:00"),DateTime("2017-01-01T00:00:00")],
[DateTime("2016-07-01T00:00:00"),DateTime("2016-10-15T00:00:00")],
[DateTime("2016-05-01T00:00:00"),DateTime("2016-08-21T00:00:00")],
]

productionwells = ["CdV-16-4ip","CdV-16-1(i)","CdV-9-1(i)"]

if isdir(joinpath(basedir,mydir,"figures"))
else
    mkdir(joinpath(basedir,mydir,"figures"))
end 

if isdir(joinpath(basedir,mydir,"figures","pumping"))
else
    mkdir(joinpath(basedir,mydir,"figures","pumping"))
end 

#------------------------------------------------------------------------------
# First grab pumping rates
#------------------------------------------------------------------------------
mycmap = get_cmap("Paired",length(productionwells)+1)

# Grab pumping rates for all times
pumpbegintime = "2016-01-01"
pumpendtime = "2017-01-01"
db.connecttodb()
production = Dict()
for productionwell in productionwells
    i = find(productionwells .== productionwell)[1]
    production[productionwell] = Dict()
    production[productionwell]["rates"], production[productionwell]["times"] = db.getpumpingrates(productionwell, pumpbegintime, pumpendtime)
    production[productionwell]["x"], production[productionwell]["y"], production[productionwell]["r"] = db.getgeometry(productionwell)
end
db.disconnectfromdb()

for yamlfile in yamlfiles
end

#Plotting
for i in 1:length(yamlfiles)
    welldir =  split(yamlfiles[i],"/")[1]
    fname = split(yamlfiles[i],"/")[2]
    wellname = split(fname,".")[1]

#=
    # First method: filtered waterlevels
    spoint = wp.SPoint(joinpath(basedir,mydir,"forward",welldir,"$(wellname).s_point"))
    spwellname = spoint[:getwellname]()
    waterlevelinfo = wl.waterlevelinfo(joinpath(basedir,mydir,yamlfiles[i]))
    ax2 = ax[:twinx]() # Create another axis on top of the current axis
    ax2[:plot](waterlevelinfo[spwellname]["times"],waterlevelinfo[spwellname]["waterlevels"]*3.28084,c="grey",marker=".",ls="",label = productionwells[i])
=#
    # Second method: raw waterlevels
    db.connecttodb()
    waterlevels, waterleveltimes = db.getwaterlevels(wellinfo[i][1], wellinfo[i][2], pumpbegintime, pumpendtime)
    db.disconnectfromdb()

    fig, ax = plt.subplots(figsize = (11,5),sharex=true)
    ax[:plot](waterleveltimes,waterlevels*3.28084,c="k",marker=".",ls="",label = productionwells[i])
    ax[:set_xlim]([datelimits[i][1],datelimits[i][2]])
    ax[:set_ylabel]("Water Level [ft]")

    ax2 = ax[:twinx]() # Create another axis on top of the current axis
    ax2[:bar](production[productionwells[i]]["times"],production[productionwells[i]]["rates"]*264.172/60/24,label = customlabels[i],color=mycmap(i),align="center",alpha=0.75)
    ax2[:set_xlim]([datelimits[i][1],datelimits[i][2]])
    ax2[:set_ylabel]("Pumping Rate [gpm]")
    
    labels = ax[:get_xticklabels]() 
    for label in labels
        label[:set_rotation](30) 
    end

    ax2[:yaxis][:set_major_formatter](mtick.FormatStrFormatter("%.1f"))
    plt.tight_layout()

    box = ax[:get_position]()
    ax[:set_position]([box[:x0], box[:y0], box[:width] * 0.85, box[:height]])

    box = ax2[:get_position]()
    ax2[:set_position]([box[:x0], box[:y0], box[:width] * 0.85, box[:height]])
    ax2[:legend](loc=4, bbox_to_anchor=(1.25, 0.0),fontsize=12)

    ax[:set_zorder](ax2[:get_zorder]()+1) # put ax in front of ax2 
    ax[:patch][:set_visible](false) # hide the 'canvas' 

    savefig(joinpath(basedir,mydir,"figures","pumping","waterLevel_pumping_$(wellname).png"),dpi=600)
    plt.close()
end
