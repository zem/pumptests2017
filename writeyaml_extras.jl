# Id: writeyaml_extras.jl, Fri 02 Feb 2018 03:47:54 PM MST pandeys #
# Created by Sachin Pandey, LANL
# Description: Write additional variables to yaml file based upon yaml files used in previous analysis.
#------------------------------------------------------------------------------
import YAML
import PyCall
@PyCall.pyimport yaml

if length(ARGS) < 2 
    println("Arguments must be of the form newdir oldir")
    println("e.g. julia calc_rsquared.jl chromium_2017 chromium_2016")
    quit()
end

newdir = ARGS[1]
olddir = ARGS[2]

include(joinpath(newdir,"wellnames.jl"))
yamlpaths = yamlfiles

for yamlpath in yamlpaths
	# YAML variables you would like to port from old Theis analysis
	myvars = ["Bad dates","Observation jumps", "Ignore production wells"]

	try 
		oldyamldata = YAML.load(open(joinpath(olddir,yamlpath)))
		newyamldata = YAML.load(open(joinpath(newdir,yamlpath))) 
		oldyamlkeys = keys(oldyamldata)

		for myvar in myvars
			if any(x->x==myvar, keys(oldyamldata))
				newyamldata[myvar] = oldyamldata[myvar]
			end
		end
		run(`rm $(joinpath(newdir,yamlpath))`)
		write(joinpath(newdir,yamlpath), yaml.dump(newyamldata, width=255))
	catch
		println("$yamlpath not found in old Theis directory")
	end
end
