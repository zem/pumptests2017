# Id: plot_waterlevel.jl, Thu 20 Dec 2016 02:27:18 PM MDT pandeys #
# Created by Sachin Pandey, LANL
# Description: Plot water levels in Julia
# - Call plot_waterleve_fun for multiple wells
#------------------------------------------------------------------------------
include("functions.jl")

basedir = pwd()

if length(ARGS) < 1
	println("Arguments must be of the form mydir")
	println("e.g. plot_waterlevels.jl cr_2016")
	quit()
end

mydir = ARGS[1]
include(joinpath(mydir,"wellnames.jl"))

for yamlfile in yamlfiles
	try
		plotWaterLevels(yamlfile,mydir)
	catch
		println("problem with $yamlfile")
	end
end

println("Finished with that")
