# Id: dd2csv.py, Thu 10 Jan 2017 03:38:38 PM MST pandeys #
# Created by Sachin Pandey, LANL
# Description: Write forward model results to csv file
#------------------------------------------------------------------------------
import datetime
import matplotlib
import matplotlib.pyplot as plt
from wellspy import SPoint
import pandas as pd
import os
import numpy as np
import yaml

def roundTime(dt=None, roundTo=60):
    """Round a datetime object to any time laps in seconds
    dt : datetime.datetime object, default now.
    roundTo : Closest number of seconds to round to, default 1 minute.
    Author: Thierry Husson 2012 - Use it as you want but don't blame me.
    """
    if dt == None : dt = datetime.datetime.now()
    seconds = (dt.replace(tzinfo=None) - dt.min).seconds
    rounding = (seconds+roundTo/2) // roundTo * roundTo
    return dt + datetime.timedelta(0,rounding-seconds,-dt.microsecond)

def datetimestring2datetime(datetimestr):
	datestr, timestr = datetimestr.split()
	datesplit = datestr.split("-")
	year, month, day = map(int, datesplit)
	timesplit = timestr.split(":")
	hour, minute, second = map(int, timesplit)
	return datetime.datetime(year, month, day, hour, minute, second)

def string2datetime(datetimestr):
	# '2007-03-01T00:00:00'
	year = int(datetimestr.split("T")[0].split("-")[0])
	month = int(datetimestr.split("T")[0].split("-")[1])
	day = int(datetimestr.split("T")[0].split("-")[2])
	hour = int(datetimestr.split("T")[1].split(":")[0])
	minute = int(datetimestr.split("T")[1].split(":")[1])
	second = int(datetimestr.split("T")[1].split(":")[2])
	return datetime.datetime(year, month, day, hour, minute, second)

def datestring2datetime(datetimestr):
	# '2007-03-01T00:00:00'
	year = int(datetimestr.split("-")[0])
	month = int(datetimestr.split("-")[1])
	day = int(datetimestr.split("-")[2])
	return datetime.datetime(year, month, day)

def dd2csv(obs_well_name,csvdir):
    print('starting obs well '+ obs_well_name)

    spointfilename = os.path.join("./forward_samestart_waffle",obs_well_name)
    spoint = SPoint(spointfilename)
    spointwaterleveltimes = spoint.getwaterleveltimes()
    spointwaterlevels = spoint.getwaterlevels()

    ddnames = spoint.getdrawdownnames()
    # ddnames.remove('total')
    # ddnames.remove('trend')
    drawdowns = spoint.getdrawdowns()

    # Maximum d.d. that occurred between the times ~11/01/15 - 01/01/16 as estimated by the theis model.
    date_min = datetime.datetime.strptime('2007-03-01', '%Y-%m-%d')
    date_max = datetime.datetime.strptime('2018-01-01', '%Y-%m-%d')

    # First grab the times
    id_newdates = [i for i,v in enumerate(spointwaterleveltimes) if v >= date_min and v < date_max]
    # newtimes = [roundTime(spointwaterleveltimes[i]) for i in id_newdates] # for debugging (datetimes not string for plot)
    newtimes = [str(roundTime(spointwaterleveltimes[i])).replace(" ","T") for i in id_newdates]

    df = pd.DataFrame(index=newtimes,columns=ddnames)
    # append water levels
    newwaterlevels = [spointwaterlevels[i] for i in id_newdates]
    df.loc[:,"waterlevel"]= newwaterlevels
    # df.plot(); # for debugging

    # append drawdowns
    for ddname in ddnames:
    # method 1: minimize by mindd
    #mindd = min(drawdowns[ddnames[id_ddwell[0]]])
    #dd_temp =  map(lambda dd: dd - mindd, drawdowns[ddnames[id_ddwell[0]]])
    #id_newdates = [i for i,v in enumerate(spointwaterleveltimes) if v > date_min and v < date_max]
    #newwaterlevels = [dd_temp[i] for i in id_newdates]
    #df.loc[:,ddname]=newwaterlevels

    # method 2: just use the raw output s_point
      newdds = [drawdowns[ddname][i] for i in id_newdates]
      df.loc[:,ddname]= newdds

    ## ------------ FOR DAN, ADD COLUMN OF ZEROS AND ONES ------------ ##
    ## These correspond with times that were not used for calibration
    nrows = len(df)

    # open up yamlfile used for calibration
    welldir = obs_well_name.split("/")[0]
    wellname = obs_well_name.split("/")[1].split(".")[0]

    controlfilename = welldir+"/"+wellname+".yaml"
    with open(controlfilename, "r") as f:
    	controldata = yaml.load(f)
    	f.close()

    observationbegintime = datestring2datetime(controldata["Observation begin time"])
    endtime = controldata["End time"]
    try:
    	allbaddates = map(lambda x: [datetimestring2datetime(x[0]), datetimestring2datetime(x[1])], controldata["Bad dates"])
    except KeyError:
    	allbaddates = []

    # tag simulated dds before first observation point
    calwaterlevels = np.zeros(len(newtimes))
    for i in range(len(newtimes)):
    	if string2datetime(newtimes[i])>=observationbegintime:
    		calwaterlevels[i]=1

    # tag simulated dds during bad dates that occurs during a single day
    for baddates in allbaddates:
    	for i in range(len(newtimes)):
    		temp = string2datetime(newtimes[i])
    		if datetime.date(temp.year, temp.month, temp.day)==datetime.date(baddates[0].year,baddates[0].month,baddates[0].day):
    			calwaterlevels[i]=0

    # tag simulated dds during bad dates that range multiple days
    for baddates in allbaddates:
    	for i in range(len(newtimes)):
    		if string2datetime(newtimes[i])>=baddates[0] and string2datetime(newtimes[i])<baddates[1]:
    			calwaterlevels[i]=0

    # tag simulated dds after last observation point
    spointfilename2 = os.path.join("./",obs_well_name)
    spoint2 = SPoint(spointfilename2)
    spointwaterleveltimes2 = spoint2.getwaterleveltimes()

    for baddates in allbaddates:
    	for i in range(len(newtimes)):
    		if string2datetime(newtimes[i])>=spointwaterleveltimes2[-1]:
    			calwaterlevels[i]=0

    df['target'] = pd.Series(calwaterlevels, index=df.index)
    ## ------------ END FOR DAN, ADD COLUMN OF ZEROS AND ONES ------------ ##

    # # Select only midnight hour every day
    # # Easier to manipulate columns than indices in pandas
    # df['temp'] = df.index
    # df = df[df['temp'].str.contains('00:00:00')]
    # df = df.drop('temp',1)

    # Write to csv
    df.to_csv(os.path.join(csvdir, obs_well_name.split("/")[1].split(".")[0] + ".csv"))

csvdir = "./ddcsv"

obs_well_names = [
            "CrEX-1/CrEX-1#1.s_point",
            "CrEX-3/CrEX-3.s_point",
            "CrPZ-1/CrPZ-1.s_point",
            "CrPZ-2/CrPZ-2#1.s_point",
            "CrPZ-3/CrPZ-3.s_point",
            "CrPZ-4/CrPZ-4.s_point",
            "CrPZ-5/CrPZ-5.s_point",
            "R-01/R-01.s_point",
            "R-11/R-11.s_point",
            "R-13/R-13.s_point",
            "R-15/R-15.s_point",
            "R-28/R-28.s_point",
            "R-33/R-33#1.s_point",
            "R-33/R-33#2.s_point",
            "R-35a/R-35a.s_point",
            "R-35b/R-35b.s_point",
            "R-36/R-36.s_point",
            "R-42/R-42.s_point",
            "R-43/R-43#1.s_point",
            "R-43/R-43#2.s_point",
            "R-44/R-44#1.s_point",
            "R-44/R-44#2.s_point",
            "R-45/R-45#1.s_point",
            "R-45/R-45#2.s_point",
            "R-50/R-50#1.s_point",
            "R-50/R-50#2.s_point",
            "R-61/R-61#1.s_point",
            "R-61/R-61#2.s_point",
            "R-62/R-62.s_point",
            "SIMR-2/SIMR-2.s_point",
            ]

# obs_well_name = "CrEX-3/CrEX-3.s_point" # for debugging

if os.path.isdir(csvdir):
    print("csv directory exists")
else:
    print("creating csv directory")
    os.makedirs(csvdir)

for obs_well_name in obs_well_names:
    dd2csv(obs_well_name,csvdir)
