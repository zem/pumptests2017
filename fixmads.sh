#!/bin/bash

## declare an array variable
#
if [ "$1" != "" ]; then
    declare -a arr=("$1")
else
    declare -a arr=("CrPZ-2/CrPZ-2#1.mads" "R-62/R-62.mads" "R-44/R-44#1.mads" "R-45/R-45#2.mads" "R-50/R-50#1.mads" "R-50/R-50#2.mads" "R-61/R-61#1.mads" "R-62/R-62.mads" "SIMR-2/SIMR-2.mads" "CrPZ-1/CrPZ-1.mads")    
fi

## now loop through the above array
for i in "${arr[@]}"
do
   echo "$i"
   sed -i '/S_O-04/c\- S_O-04: {init: -2.5, init_max: -2, init_min: -6, log: false, max: -2, min: -6, step: 0.1, type: opt}' $i
#   sed -i '/S_PM-04/c\- S_PM-04: {init: -2.5, init_max: -2, init_min: -6, log: false, max: -2, min: -6, step: 0.1, type: opt}' $i
#   sed -i '/S_PM-05/c\- S_PM-05: {init: -2.5, init_max: -2, init_min: -6, log: false, max: -2, min: -6, step: 0.1, type: opt}' $i
#   sed -i '/S_PM-02/c\- S_PM-02: {init: -2.5, init_max: -2, init_min: -6, log: false, max: -2, min: -6, step: 0.1, type: opt}' $i
#   sed -i '/K_PM-01/c\- K_PM-01: {init: 10, init_max: 10, init_min: -1, log: false, max: 10, min: -6, step: 0.1, type: null}' $i
#   sed -i '/K_PM-03/c\- K_PM-03: {init: 10, init_max: 10, init_min: -1, log: false, max: 10, min: -6, step: 0.1, type: null}' $i
#   sed -i '/S_PM-03/c\- S_PM-03: {init: -10, init_max: -0.5, init_min: -10, log: false, max: 0, min: -10, step: 0.1, type: null}' $i
#   sed -i '/S_PM-01/c\- S_PM-01: {init: -10, init_max: -0.5, init_min: -10, log: false, max: 0, min: -10, step: 0.1, type: null}' $i
done

## declare an array variable
#declare -a arr2=("R-44/R-44#1.mads" "R-50/R-50#1.mads" "R-50/R-50#2.mads" "R-62/R-62.mads" "SIMR-2/SIMR-2.mads")

## now loop through the above array
#for i in "${arr2[@]}"
#do
#   echo "$i"
#	sed -i '/S_CrIN-3/c\- S_CrIN-3: {init: -3, init_max: -2, init_min: -4, log: false, max: 0, min: -6, step: 0.1, type: opt}' chromium_2017/$i
#	sed -i '/S_CrEX-3/c\- S_CrEX-3: {init: -3, init_max: -2, init_min: -4, log: false, max: 0, min: -6, step: 0.1, type: opt}' chromium_2017/$i
#	sed -i '/S_CrEX-1/c\- S_CrEX-1: {init: -3, init_max: -2, init_min: -4, log: false, max: 0, min: -6, step: 0.1, type: opt}' chromium_2017/$i
#	sed -i '/K_CrIN-3/c\- K_CrIN-3: {init: 2.5, init_max: 3, init_min: 2, log: false, max: 8, min: -6, step: 0.1, type: opt}' chromium_2017/$i
#	sed -i '/K_CrEX-3/c\- K_CrEX-3: {init: 2.5, init_max: 3, init_min: 2, log: false, max: 8, min: -6, step: 0.1, type: opt}' chromium_2017/$i
#	sed -i '/K_CrEX-1/c\- K_CrEX-1: {init: 2.5, init_max: 3, init_min: 2, log: false, max: 8, min: -6, step: 0.1, type: opt}' chromium_2017/$i
#done




