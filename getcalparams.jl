# Id: getcalparams.jl, Thu 05 Jan 2017 05:50:41 PM MST pandeys #
# Created by Sachin Pandey, LANL
# Description: Get paramaters from rerun-rerun.mads and write to text file.
#------------------------------------------------------------------------------
import Mads

function getparams(yamlfile)
  @show yamlfile
  # Initialize
  welldir =  split(yamlfile,"/")[1]
  fname = split(yamlfile,"/")[2]
  wellname = split(fname,".")[1]

  mdopt = Mads.loadmadsfile(joinpath(basedir,mydir,welldir,"$wellname-rerun-rerun.mads"))
  params = Mads.getparamsinit(mdopt)
  mykeys = Mads.getparamkeys(mdopt)

  outfile = open(joinpath(basedir,mydir,welldir,"$wellname.params"), "w")
  for i in 1:length(params)
     println(outfile,"$(mykeys[i]): $(params[i])")
  end
  close(outfile)
end

basedir = pwd()

if length(ARGS) < 1
    println("Arguments must be of the form mydir")
    println("e.g. julia getcalparams.jl chromium_2016")
    quit()
end

mydir = ARGS[1]
include(joinpath(mydir,"wellnames.jl"))

for yamlfile in yamlfiles
  getparams(yamlfile)
end
