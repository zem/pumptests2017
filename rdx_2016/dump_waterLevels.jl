import sachFun

basedir = "/lclscratch/sach/pumptests2016"
mydir = "rdx_2016_report"
begintime = "2016-06-01"

include(joinpath(basedir,mydir,"wellnames.jl"))

if isdir(joinpath(basedir,mydir,"waterlevels_raw"))
else
    mkdir(joinpath(basedir,mydir,"waterlevels_raw"))
end 

for yamlfile in yamlfiles
    fname = split(yamlfile,"/")[end]
    wellname = split(fname,".")[1]
    welldir =  replace(yamlfile,fname,"")
    
    waterleveltimes,waterlevels = sachFun.rawWaterLevels(joinpath(basedir,mydir),yamlfile)
    outfile = open("./waterlevels_raw/waterlevels_$(wellname).csv", "w")
    for i in 1:length(waterleveltimes)
        timestringarray = split(string(waterleveltimes[i]),"T")
        print(outfile, "$(timestringarray[1]) $(timestringarray[2]),")
#        print(outfile, "$(split(string(waterleveltimes[i]),"T")[2]),")        
        @printf(outfile, "%0.5e \n", waterlevels[i])
    end
    close(outfile)
end
