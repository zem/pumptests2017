# Id: forward_cal_waffle.jl, Wed 04 Jan 2017 01:26:53 PM MST pandeys #
# Created by Sachin Pandey, LANL
# Description: Run calibrated forward model and plot results (no gaps in model run)
#   - This is a modified version of forward_cal_samestart.jl that modifies the 
#         yaml file so that all model runs have the same pump begin time AND 
#         the same observation begin time. 
#   - The model outputs can then be used as observations for waffle model calibration.
#         Observations for waffle calibration are created using dd2csv.py
#------------------------------------------------------------------------------
import Mads

function wellsforward_nobaddates_samestart(yamlfile)
    # Initialize
    basedir = pwd()
    welldir =  split(yamlfile,"/")[1]
    fname = split(yamlfile,"/")[2]
    wellname = split(fname,".")[1]

    # Move appropriate yaml file to forward run directory
    if !isdir(joinpath(basedir,mydir,"forward_samestart_waffle"))
        mkdir(joinpath(basedir,mydir,"forward_samestart_waffle"))
    end

    cd(joinpath(basedir,mydir,"forward_samestart_waffle"))

    if !isdir(welldir)
        mkdir(welldir)
    end

    cd(joinpath(basedir,mydir,"forward_samestart_waffle",welldir))
    # run(`rm *`)
    cp(joinpath("../..",yamlfile),fname,remove_destination=true)

    # Replace start time so that all forward runs have the same date range
    newobsbegintime = newpumpbegintime
    yamldata = YAML.load(open("./$(fname)"))
    obsbegintime = yamldata["Observation begin time"]
    pumpbegintime = yamldata["Pumping begin time"]
    run(`find ./ -name $(fname) -exec sed -i "s/Observation begin time: '$(obsbegintime)'/Observation begin time: '$(newobsbegintime)'/g" {} \;`) 
    run(`find ./ -name $(fname) -exec sed -i "s/Pumping begin time: '$(pumpbegintime)'/Pumping begin time: '$(newpumpbegintime)'/g" {} \;`) 

    # Run setupmads_noBadDates
    cd(basedir)
    run(`pipenv run python setupmads_nobaddates_revert.py $(joinpath(mydir,"forward_samestart_waffle",yamlfile))`)

    ### ---------- REPLACE TIMES IN .TPL FILE ---------- ###
    f = open(joinpath(mydir,"forward_samestart_waffle",welldir,"$(wellname).tpl"))
    lines = readlines(f)
    # mykey = "--- Number of points: 1\n"
	mykey = "--- Number of points: 1"
    istart = find(x -> x == mykey,lines)
    close(f)

    date0 = DateTime(2000, 1, 1)
    newobsbegintime_datetime = DateTime(newobsbegintime)
    endtime_datetime = DateTime(yamldata["End time"])
    dt_days = convert(Dates.Day,endtime_datetime-newobsbegintime_datetime)

    starttime_float = convert(Float64,convert(Dates.Day,newobsbegintime_datetime-date0))
    endtime_float = convert(Float64,convert(Dates.Day,endtime_datetime-date0))-1
    ndates = convert(Int,endtime_float-starttime_float)
    
    newlines = Array{String,1}()
    for line in lines[1:istart[1]]
        newlines = append!(newlines,[line*"\n"])
    end

    npointsline = lines[istart+1]
    temp1 = split(npointsline[1],"\t")[1] # wellname
    temp2 = split(npointsline[1],"\t")[2] # float
    temp3 = split(npointsline[1],"\t")[3] # float
    newnpointsline = "$temp1\t$temp2\t$temp3\t$(ndates+1)\n"
    newlines = append!(newlines,[newnpointsline])
    newlines = append!(newlines,[lines[istart[1]+2]*"\n"])
    newlines = append!(newlines,[lines[istart[1]+3]*"\n"])

    datefloats = test = starttime_float:1:endtime_float

    for datefloat in datefloats
        newlines = append!(newlines,["$(datefloat)\n"])
    end

    run(`rm $(joinpath(mydir,"forward_samestart_waffle",welldir,"$(wellname).tpl"))`)
    outfile = open(joinpath(mydir,"forward_samestart_waffle",welldir,"$(wellname).tpl"), "w")
    for newline in newlines
      print(outfile,newline)
    end
    close(outfile)
    ### ---------- END REPLACE TIMES IN .TPL FILE ---------- ###

    # Replace initial conditions with calibrated values
    mdopt = Mads.loadmadsfile(joinpath(mydir,welldir,"$wellname-rerun-rerun.mads"))
    params = Mads.getparamsinit(mdopt)
    keys = Mads.getparamkeys(mdopt)
    mdbd = Mads.loadmadsfile(joinpath(mydir,"forward_samestart_waffle",welldir,"$wellname.mads")) # Use old parameters as search criteria
    params_init = Mads.getparamsinit(mdbd)

    cd(joinpath(basedir,mydir,"forward_samestart_waffle",welldir))
    # Replace floats
    for i in 1:length(keys)
        run(`find ./ -name $wellname.mads -exec sed -i "s/- $(keys[i]): {init: $(params_init[i]),/- $(keys[i]): {init: $(params[i]),/g" {} \;`)    
    end

    # Replace integers
    for i in 1:length(keys)
        run(`find ./ -name $wellname.mads -exec sed -i "s/- $(keys[i]): {init: $(Int(round(params_init[i]))),/- $(keys[i]): {init: $(params[i]),/g" {} \;`)    
    end

    # Run mads forward
    cd(basedir)
    run(`mads16 $(joinpath(mydir,"forward_samestart_waffle",welldir,"$wellname.mads")) forward`)
    
    # ### ---------- REMOVE BAD TIMES FROM SPOINT FILE ---------- ###
    # f = open(joinpath(mydir,"forward_samestart_waffle",welldir,"$(wellname).s_point"))
    # lines = readlines(f)
    # mykey = "0.000 "
    # igood = find(x -> x[1:6] != mykey,lines)
    # newlines = lines[igood]
    # close(f)
    # 
    # run(`rm $(joinpath(mydir,"forward_samestart_waffle",welldir,"$(wellname).s_point"))`)
    # outfile = open(joinpath(mydir,"forward_samestart_waffle",welldir,"$(wellname).s_point"), "w")
    # for newline in newlines
    #   print(outfile,newline)
    # end
    # close(outfile)
    # ### ---------- END REMOVE BAD TIMES FROM SPOINT FILE ---------- ###
    
    # Make plot! (from main directory)
    run(`python plot_theis_driver.py $(joinpath(mydir,"forward_samestart_waffle",welldir,"$wellname.yaml"))`)
    
    cd(basedir)
end

# for debug
mydir = "chromium_2017"
newpumpbegintime = "2010-01-01"
yamlfile = "R-45/R-45#1.yaml"

if length(ARGS) < 2 
    println("Arguments must be of the form mydir date")
    println("e.g. julia forward_cal_samestart.jl chromium_2016 2007-03-01")
    quit()
end

mydir = ARGS[1]
newpumpbegintime = ARGS[2]
include(joinpath(mydir,"wellnames.jl"))
for yamlfile in yamlfiles
    try
        wellsforward_nobaddates_samestart(yamlfile)
        println("Success: $yamlfile")
    catch
    	println("Error: $yamlfile")
    end
end
