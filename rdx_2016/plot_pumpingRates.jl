# Id: plot_pumpingRates.jl, Thu 22 Dec 2016 11:04:11 AM MST pandeys #
# Created by Sachin Pandey, LANL
# Description: Plot water levels and rates together
#------------------------------------------------------------------------------
using YAML
using DataFrames
using PyPlot
plt = PyPlot
import PyCall
@PyCall.pyimport aquiferdb as db
@PyCall.pyimport chipbeta as cb
@PyCall.pyimport wellspy as wp
@PyCall.pyimport waterlevelinfo as wl

productionwells = ["CdV-16-4ip","CdV-16-1(i)","CdV-9-1(i)"]
customlabels = ["CdV-16-4ip","CdV-16-1i","CdV-9-1i"]
plotbar = true
pumpbegintime = "2016-06-01"
pumpendtime =  "2016-10-30"

if isdir(joinpath("figures"))
else
    mkdir(joinpath("figures"))
end 

if isdir(joinpath("figures","pumping"))
else
    mkdir(joinpath("figures","pumping"))
end 

#------------------------------------------------------------------------------
# First grab pumping rates
#------------------------------------------------------------------------------
mycmap = get_cmap("Paired",length(productionwells)+1)

# Grab pumping rates for all times
db.connecttodb()
production = Dict()
for productionwell in productionwells
    i = find(productionwells .== productionwell)[1]
    production[productionwell] = Dict()
    production[productionwell]["rates"], production[productionwell]["times"] = db.getpumpingrates(productionwell, pumpbegintime, pumpendtime)
    production[productionwell]["x"], production[productionwell]["y"], production[productionwell]["r"] = db.getgeometry(productionwell)
end
db.disconnectfromdb()

#------------------------------------------------------------------------------
# First Plot: Pumping
#------------------------------------------------------------------------------
# Initialize figure
fig, ax = plt.subplots(figsize = (11,5),sharex=true)

# Plot data
for productionwell in productionwells
    i = find(productionwells .== productionwell)[1]
    ## create datafram to easily sort by time for plotting
    df = DataFrame(times = production[productionwell]["times"],rates = production[productionwell]["rates"])
    # b = bar(df[:times],df[:rates]*264.172/60/24,label = productionwell,color=mycmap(i))
    if plotbar
        ax[:bar](df[:times],df[:rates]*264.172/60/24,label = customlabels[i],color=mycmap(i),align="center")
    else
        ax[:plot](df[:times],df[:rates]*264.172/60/24,label = customlabels[i],marker="o",c=mycmap(i))        
    end
end
ax[:set_xlim]([DateTime("$(pumpbegintime)T00:00:00"),DateTime("$(pumpendtime)T00:00:00")])
ax[:set_ylabel]("Pumping Rate [gpm]")

labels = ax[:get_xticklabels]() 
for label in labels
    label[:set_rotation](30) 
end

plt.tight_layout()
box = ax[:get_position]()
ax[:set_position]([box[:x0], box[:y0], box[:width] * 0.85, box[:height]])

handles, labels = ax[:get_legend_handles_labels]()
handles = [handles[3]  ,handles[2],handles[1]]
labels =  ["CdV-9-1i","CdV-16-1i" ,"CdV-16-4ip"]
ax[:legend](handles,labels,loc=4, bbox_to_anchor=(1.2, 0.0),fontsize=11)

if plotbar
    savefig(joinpath("figures","pumping","pumpingRates_bar.png"),dpi=600)
else
    savefig(joinpath("figures","pumping","pumpingRates.png"))
end

plt.close()
