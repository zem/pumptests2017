# Id: plot_pumpingRates.jl, Thu 22 Dec 2016 11:04:11 AM MST pandeys #
# Created by Sachin Pandey, LANL
# Description: Plot water levels and rates together
#------------------------------------------------------------------------------
using YAML
using DataFrames
using PyPlot
plt = PyPlot
import PyCall
@PyCall.pyimport aquiferdb as db
@PyCall.pyimport chipbeta as cb
@PyCall.pyimport wellspy as wp
@PyCall.pyimport waterlevelinfo as wl
import DataFrames
df = DataFrames

if length(ARGS) < 3 
println("Arguments must be of the form mydir begintime endtime")
println("e.g. julia plot_production.jl chromium_2017 2001-01-01 2018-01-01")
quit()
end

mydir = ARGS[1]
pumpbegintime = ARGS[2]
pumpendtime = ARGS[3]

include(joinpath(mydir,"wellnames.jl"))

if isdir(joinpath(mydir,"figures"))
else
mkdir(joinpath(mydir,"figures"))
end 

if isdir(joinpath(mydir,"figures","production"))
else
mkdir(joinpath(mydir,"figures","production"))
end 

#------------------------------------------------------------------------------
# First grab pumping rates
#------------------------------------------------------------------------------
mycmap = get_cmap("Paired",length(productionwells)+1)

# Grab pumping rates for all times
db.connecttodb()
production = Dict()
for productionwell in productionwells
i = find(productionwells .== productionwell)[1]
production[productionwell] = Dict()
production[productionwell]["rates"], production[productionwell]["times"] = db.getpumpingrates(productionwell, pumpbegintime, pumpendtime)
production[productionwell]["x"], production[productionwell]["y"], production[productionwell]["r"] = db.getgeometry(productionwell)
end
db.disconnectfromdb()

#------------------------------------------------------------------------------
# First Plot: Pumping
#------------------------------------------------------------------------------
# Initialize figure
fig, ax = plt.subplots(figsize = (11,5),sharex=true)

# Plot data
for productionwell in productionwells
i = find(productionwells .== productionwell)[1]
if length(production[productionwell]["times"]) > 0
	results = df.DataFrame(times = production[productionwell]["times"],rates = production[productionwell]["rates"])
	newresults = df.DataFrame(times=DateTime[],rates=Float64[])

for j in 1:length(results[:,1])-1
		push!(newresults,[results[j,:][:times][1],results[j,:][:rates][1]])
		push!(newresults,[results[j+1,:][:times][1],results[j,:][:rates][1]])
end

if results[end,:][:rates][1] == 0.0
		push!(newresults,[results[end,:][:times][1],results[end,:][:rates][1]])
end

if results[1,:][:rates][1] != 0.0
		results = append!(df.DataFrame(times=results[1,:][:times][1],rates=0.0),newresults)
else
		results = newresults
end

ax[:plot](results[:times],results[:rates]*264.172/60/24,label = productionwell,color=mycmap(i),linewidth=1.5)
end
end
# ax[:set_xlim]([DateTime("$(pumpbegintime)T00:00:00"),DateTime("$(pumpendtime)T00:00:00")])
ax[:set_ylabel]("Pumping Rate [gpm]")

labels = ax[:get_xticklabels]() 
for label in labels
label[:set_rotation](30) 
end

plt.tight_layout()
box = ax[:get_position]()
ax[:set_position]([box[:x0], box[:y0], box[:width] * 0.85, box[:height]])
ax[:legend](loc=4, bbox_to_anchor=(1.2, 0.0),fontsize=12)

savefig(joinpath(mydir,"figures","production","production_$(ARGS[2])_$(ARGS[3]).png"))

# plt.close()
