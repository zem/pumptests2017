# Id: waterlevelinfo.py, Wed 25 Jan 2017 05:26:26 PM MST pandeys #
# Created by Sachin Pandey, LANL
# Description: Get waterlevelinfo for various observation wellspy
#  - I made this so that it can be called from Julia, e.g.
#       import PyCall
#       @PyCall.pyimport waterlevelinfo as wl
#       waterlevelinfo = wl.waterlevelinfo("R-15/R-15.yaml")
#  - I made a script to do the data-processing in Julia, but I had issues.
#------------------------------------------------------------------------------
import yaml
import datetime
import aquiferdb as db
import chipbeta as cb
import wellspy
import os

def datetimestring2datetime(datetimestr):
	datestr, timestr = datetimestr.split()
	datesplit = datestr.split("-")
	year, month, day = map(int, datesplit)
	timesplit = timestr.split(":")
	hour, minute, second = map(int, timesplit)
	return datetime.datetime(year, month, day, hour, minute, second)

def datetime2datetimestring(dt):
	return datetime2datestring(dt) + " " + ":".join(map(lambda i: str(i).zfill(2), [dt.hour, dt.minute, dt.second]))

def datestring2datetime(datestr):
	splitstr = datestr.split("-")
	return datetime.datetime(*map(int, splitstr))

def datetime2datestring(dt):
	return "-".join(map(lambda i: str(i).zfill(2), [dt.year, dt.month, dt.day]))


def waterlevelinfo(yamlfile):
	rootname = ".".join(yamlfile.split(".")[:-1])
	
	controlfilename = yamlfile
	with open(controlfilename, "r") as f:
		controldata = yaml.load(f)
		f.close()
	
	# productionwells = controldata["Production wells"]
	observationwell = controldata["Observation well"]
	observationbegintime = controldata["Observation begin time"]
	try:
		#portdescr = "P" + str(controldata["Screen number"]) + "A"
		portdescr = str(controldata["Screen number"])
	except KeyError:
		portdescr = "SINGLE COMPLETION"
	try:
		pumpingbegintime = controldata["Pumping begin time"]
	except KeyError:
		dt = datestring2datetime(observationbegintime)
		pumpingbegintime = datetime2datestring(dt - datetime.timedelta(days=365))
	endtime = controldata["End time"]
	barometricbegintime = datetime2datestring(datestring2datetime(observationbegintime) + datetime.timedelta(days=-1))
	barometricendtime = datetime2datestring(datestring2datetime(endtime) + datetime.timedelta(days=1))
	try:
		jumpfixdates = map(lambda x: datetimestring2datetime(x[0]), controldata["Observation jumps"])
		jumpfixsize = map(lambda x: x[1], controldata["Observation jumps"])
	except KeyError:
		jumpfixdates = []
		jumpfixsize = []
	try:
		samplethreshold = controldata["Sample threshold"]
	except KeyError:
		samplethreshold = 0.08
	try:
		samplelength = controldata["Sample length"]
	except KeyError:
		samplelength = 5
	try:
		allbaddates = map(lambda x: [datetimestring2datetime(x[0]), datetimestring2datetime(x[1])], controldata["Bad dates"])
	except KeyError:
		allbaddates = []
	
	# # GOAL: shut off calibration of certain params to model zero drawdowns for certain wells.
	# # - Introduce "dummyprodwells" (YAML variable "Ignore production wells")
	# try:
	# 	dummyprodwells = controldata["Ignore production wells"]
	# except KeyError:
	# 	dummyprodwells = []
	
	db.connecttodb()
	
	#get the water level information
	waterlevels, waterleveltimes = db.getwaterlevels(observationwell, portdescr, observationbegintime, endtime)
	#remove the bad dates
	for baddates in allbaddates:
		goodwaterlevels = []
		goodwaterleveltimes = []
		for i in range(len(waterlevels)):
			if not(baddates[0] < waterleveltimes[i] and waterleveltimes[i] < baddates[1]):
				goodwaterlevels.append(waterlevels[i])
				goodwaterleveltimes.append(waterleveltimes[i])
		waterlevels = goodwaterlevels
		waterleveltimes = goodwaterleveltimes
	#fix the jumps in the water levels
	j = 0
	for jumpfixdate in jumpfixdates:
		i = 0
		while i < len(waterleveltimes) and waterleveltimes[i] < jumpfixdate:
			i += 1
		while i < len(waterleveltimes):
			waterlevels[i] += jumpfixsize[j]
			i += 1
		j += 1
		
	# 
	# #get the production information
	# production = {}
	# for productionwell in productionwells:
	# 	production[productionwell] = {}
	# 	production[productionwell]["rates"], production[productionwell]["times"] = db.getpumpingrates(productionwell, pumpingbegintime, endtime)
	# 	production[productionwell]["x"], production[productionwell]["y"], production[productionwell]["r"] = db.getgeometry(productionwell)
	
	#get the barometric information
	baropressmb, barotimes = db.getbarometricpressure(barometricbegintime, barometricendtime)
	
	#get the earth tide information -- for now, just read it from a file
	earthtideforces, earthtidetimes = db.getearthtide(barometricbegintime, barometricendtime)
	
	waterlevelsnosamp, waterleveltimesnosamp = cb.removeSamplingEvents(waterlevels, waterleveltimes, threshold=samplethreshold, samplinglength=samplelength)
	
	#goodwaterlevels, goodwaterleveltimes = cb.denoise(waterlevels, waterleveltimes, baropressmb, barotimes, earthtideforces, earthtidetimes)
	nosampwaterlevels, nosampwaterleveltimes = cb.denoise(waterlevelsnosamp, waterleveltimesnosamp, baropressmb, barotimes, earthtideforces, earthtidetimes)
	
	#remove the bad dates from the no samp water levels
	waterlevels = nosampwaterlevels
	waterleveltimes = nosampwaterleveltimes
	for baddates in allbaddates:
		goodwaterlevels = []
		goodwaterleveltimes = []
		for i in range(len(waterlevels)):
			if not(baddates[0] < waterleveltimes[i] and waterleveltimes[i] < baddates[1]):
				goodwaterlevels.append(waterlevels[i])
				goodwaterleveltimes.append(waterleveltimes[i])
		waterlevels = goodwaterlevels
		waterleveltimes = goodwaterleveltimes
	nosampwaterlevels = waterlevels
	nosampwaterleveltimes = waterleveltimes
	
	waterlevelinfo = {}
	waterlevelinfo[observationwell] = {}
	waterlevelinfo[observationwell]["times"] = nosampwaterleveltimes
	waterlevelinfo[observationwell]["waterlevels"] = nosampwaterlevels
	waterlevelinfo[observationwell]["x"], waterlevelinfo[observationwell]["y"], waterlevelinfo[observationwell]["r"] = db.getgeometry(observationwell)
	
	return waterlevelinfo
