# Id: plot_cal_forward.jl, Thu 22 Dec 2016 11:04:11 AM MST pandeys #
# Created by Sachin Pandey, LANL
# Description: Plot results of forward model runs.
#------------------------------------------------------------------------------
using YAML
import DataFrames
df = DataFrames
using PyPlot
plt = PyPlot
import PyCall
@PyCall.pyimport aquiferdb as db
@PyCall.pyimport chipbeta as cb
@PyCall.pyimport wellspy as wp
@PyCall.pyimport waterlevelinfo as wl
include("functions.jl")

if length(ARGS) < 1 
	println("Arguments must be of the form mydir")
	println("e.g. julia plt_cal_forward.jl chromium_2016")
	quit()
end

basedir = pwd()

mydir = ARGS[1]
include(joinpath(mydir,"wellnames.jl"))

if isdir(joinpath(basedir,mydir,"figures"))
else
	mkdir(joinpath(basedir,mydir,"figures"))
end 

if isdir(joinpath(basedir,mydir,"figures","forward"))
else
	mkdir(joinpath(basedir,mydir,"figures","forward"))
end 

for yamlfile in yamlfiles
	fname = split(yamlfile,"/")[end]
	wellname = split(fname,".")[1]
	welldir = replace(yamlfile,fname,"")
	@show yamlfile
	# try
	#------------------------------------------------------------------------------
	# First grab pumping rates
	#------------------------------------------------------------------------------
	mycmap = get_cmap("Paired",length(productionwells)+1)

	# Grab pumping rates for all times
	pumpbegintime = "2001-01-01"
	pumpendtime = "2018-01-01"
	db.connecttodb()
	production = Dict()
	for productionwell in productionwells
		i = find(productionwells .== productionwell)[1]
		production[productionwell] = Dict()
		production[productionwell]["rates"], production[productionwell]["times"] = db.getpumpingrates(productionwell, pumpbegintime, pumpendtime)
		production[productionwell]["x"], production[productionwell]["y"], production[productionwell]["r"] = db.getgeometry(productionwell)
	end
	db.disconnectfromdb()

	#------------------------------------------------------------------------------
	# First Plot: Raw data, all times, with pumping
	#------------------------------------------------------------------------------
	# Initialize figure
	fig, ax = plt.subplots(4,1,figsize = (12.5,15))

	# Plot water levels
	waterleveltimes, waterlevels = rawWaterLevels(joinpath(mydir,yamlfile))
	ax[1][:plot](waterleveltimes,waterlevels,"k.")

	# Plot pumping
	ax2 = ax[1][:twinx]() # Create another axis on top of the current axis
	for productionwell in productionwells
		i = find(productionwells .== productionwell)[1]
		if length(production[productionwell]["times"]) > 0
			results = df.DataFrame(times = production[productionwell]["times"],rates = production[productionwell]["rates"])
			newresults = df.DataFrame(times=DateTime[],rates=Float64[])

			for j in 1:length(results[:,1])-1
				push!(newresults,[results[j,:][:times][1],results[j,:][:rates][1]])
				push!(newresults,[results[j+1,:][:times][1],results[j,:][:rates][1]])
			end

			if results[end,:][:rates][1] == 0.0
				push!(newresults,[results[end,:][:times][1],results[end,:][:rates][1]])
			end

			if results[1,:][:rates][1] != 0.0
				results = append!(df.DataFrame(times=results[1,:][:times][1],rates=0.0),newresults)
			else
				results = newresults
			end

			ax2[:plot](results[:times],results[:rates],label = productionwell,color=mycmap(i),linewidth=1.5)
		end
	end

	ax[1][:set_title]("$(wellname)\nRaw Data") # Create another axis on top of the current axis
	ax[1][:set_xlim]([minimum(waterleveltimes),maximum(waterleveltimes)])
	ax[1][:set_zorder](ax2[:get_zorder]()+1) # put ax in front of ax2
	ax[1][:patch][:set_visible](false) # hide the 'canvas'

	#------------------------------------------------------------------------------
	# Second Plot: Cleaned up data, select times, with pumping
	#------------------------------------------------------------------------------
	# Plot water levels
	waterleveltimes, waterlevels = denoiseWaterLevels(joinpath(mydir,yamlfile))
	ax[2][:plot](waterleveltimes,waterlevels,"k.")

	# Plot pumping
	ax3 = ax[2][:twinx]() # Create another axis on top of the current axis
	for productionwell in productionwells
		i = find(productionwells .== productionwell)[1]
		if length(production[productionwell]["times"]) > 0
			results = df.DataFrame(times = production[productionwell]["times"],rates = production[productionwell]["rates"])
			newresults = df.DataFrame(times=DateTime[],rates=Float64[])

			for j in 1:length(results[:,1])-1
				push!(newresults,[results[j,:][:times][1],results[j,:][:rates][1]])
				push!(newresults,[results[j+1,:][:times][1],results[j,:][:rates][1]])
			end

			if results[end,:][:rates][1] == 0.0
				push!(newresults,[results[end,:][:times][1],results[end,:][:rates][1]])
			end

			if results[1,:][:rates][1] != 0.0
				results = append!(df.DataFrame(times=results[1,:][:times][1],rates=0.0),newresults)
			else
				results = newresults
			end
			ax3[:plot](results[:times],results[:rates],label = productionwell,color=mycmap(i),linewidth=1.5)
		end
	end
	ax[2][:set_xlim]([minimum(waterleveltimes),maximum(waterleveltimes)])
	ax[2][:set_title]("Calibration Data") # Create another axis on top of the current axis
	ax[2][:set_zorder](ax3[:get_zorder]()+1) # put ax in front of ax2
	ax[2][:patch][:set_visible](false) # hide the 'canvas'

	#------------------------------------------------------------------------------
	# Calibration results, total drawdown
	#------------------------------------------------------------------------------
	spoint = wp.SPoint(joinpath(basedir,mydir,"forward",welldir,"$(wellname).s_point"))
	spwellname = spoint[:getwellname]()
	spwaterleveltimes = spoint[:getwaterleveltimes]()
	spwaterlevels = spoint[:getwaterlevels]()
	ddnames = spoint[:getdrawdownnames]()
	drawdowns = spoint[:getdrawdowns]()
	waterlevelinfo = wl.waterlevelinfo(joinpath(basedir,mydir,yamlfile))

	ax[3][:set_xlim]([minimum(waterleveltimes),maximum(waterleveltimes)])
	#ax[3][:plot](waterleveltimes,waterlevels,"k.")
	ax[3][:plot](waterlevelinfo[spwellname]["times"], waterlevelinfo[spwellname]["waterlevels"], "k.")
	ax[3][:plot](spwaterleveltimes, spwaterlevels, "r", linewidth=2)
	ax[3][:set_title]("Calibration Total Drawdown") # Create another axis on top of the current axis

	#------------------------------------------------------------------------------
	# Calibration results, individual drawdown
	#------------------------------------------------------------------------------
	mycmap2 = get_cmap("gist_ncar",length(ddnames)+1)
	ddnames_labels = map((ddname) -> replace(ddname,"0",""), ddnames) # remove 0 from well names
	mod_dd = [] # used to recalculate total

	for i in 1:length(ddnames)
		mindd = minimum(drawdowns[ddnames[i]])
		if ddnames[i] == "trend"
			ax[4][:plot](spwaterleveltimes, map((dd) -> dd - mindd, drawdowns[ddnames[i]]), label = "trend", c="k", ls="--", linewidth=1.5)
			# append!(mod_dd, (map((dd) -> dd - mindd, drawdowns[ddnames[i]])))
			mod_dd = (map((dd) -> dd - mindd, drawdowns[ddnames[i]]))
		end
	end

	# Then plot wells
	ctr_col = 1 # iterator for custom colormap
	for i in 1:length(ddnames)
		mindd = minimum(drawdowns[ddnames[i]])
		if ddnames[i] != "total" && ddnames[i] != "trend"
			# append!(mod_dd, (map((dd) -> dd - mindd, drawdowns[ddnames[i]])))
			mod_dd = [mod_dd (map((dd) -> dd - mindd, drawdowns[ddnames[i]]))]
			ax[4][:plot](spwaterleveltimes, map((dd) -> dd - mindd, drawdowns[ddnames[i]]), c=mycmap2(ctr_col), label = ddnames_labels[i], linewidth=1.5)
			ctr_col += 1
		end
	end

	# Then plot total
	# PROBLEM: Sometimes individual drawdowns are greater than total!
	# SOLUTION 1: calculate a new total, which is done in the for loop above (mod_dd) 
	total_dd = []
	for i in 1:length(mod_dd[:,1])
		append!(total_dd,sum(mod_dd[i,:]))
	end
	ax[4][:set_xlim]([minimum(waterleveltimes),maximum(waterleveltimes)])
	ax[4][:plot](spwaterleveltimes, total_dd, label = "total", c="r", linewidth=1.75)
	ax[4][:invert_yaxis]()
	ax[4][:set_title]("Calibration Individual Drawdown") # Create another axis on top of the current axis

	#------------------------------------------------------------------------------
	# Make plots pretty
	#------------------------------------------------------------------------------
	for i in 1:length(ax)
		labels = ax[i][:get_xticklabels]() 
		for label in labels
			label[:set_rotation](30) 
		end
	end

	ax[1][:set_ylabel]("Water level (m)")
	ax2[:set_ylabel]("Pumping rate, m^3/day")
	ax[2][:set_ylabel]("Water level (m)")
	ax3[:set_ylabel]("Pumping rate, m^3/day")
	ax[3][:set_ylabel]("Water level (m)")
	ax[4][:set_ylabel]("Drawdown (m)")
	ax[4][:set_xlabel]("Date")

	plt.tight_layout()

	# ax[4][:legend](loc="center left", bbox_to_anchor=(1.05, 0.5))

	box = ax[1][:get_position]()
	ax[1][:set_position]([box[:x0], box[:y0], box[:width] * 0.85, box[:height]])
	ax2[:set_position]([box[:x0], box[:y0], box[:width] * 0.85, box[:height]])

	box = ax[2][:get_position]()
	ax[2][:set_position]([box[:x0], box[:y0], box[:width] * 0.85, box[:height]])
	ax3[:set_position]([box[:x0], box[:y0], box[:width] * 0.85, box[:height]])
	ax3[:legend](loc=4, bbox_to_anchor=(1.23, 0.0),fontsize=12)

	box = ax[3][:get_position]()
	ax[3][:set_position]([box[:x0], box[:y0], box[:width] * 0.85, box[:height]])

	box = ax[4][:get_position]()
	ax[4][:set_position]([box[:x0], box[:y0], box[:width] * 0.85, box[:height]])
	ax[4][:legend](loc=4, bbox_to_anchor=(1.23, 0.0),fontsize=12)

	plt.savefig(joinpath(basedir,mydir,"figures","forward","$(wellname).png"))
	plt.close()

	# catch
	# println("problems with $yamlfile")
	# end
end
