# Id: forward_cal_modpumpstart.jl, Wed 04 Jan 2017 01:26:53 PM MST pandeys #
# Created by Sachin Pandey, LANL
# Description: Run calibrated forward model and plot results
#   - This is a modified version of forward_cal.jl that modifies the yaml file 
#         so that all model runs have the same pump begin time.
#   - Use: This is used to compare maximum drawdowns over the same pumping time 
#         period. These values are determined using maxdd.py and can be used in 
#         reports.
#------------------------------------------------------------------------------
import Mads

function wellsforward_nobaddates_samestart(yamlfile)
    # Initialize
    basedir = pwd()
    welldir =  split(yamlfile,"/")[1]
    fname = split(yamlfile,"/")[2]
    wellname = split(fname,".")[1]

    # Move appropriate yaml file to forward run directory
    if !isdir(joinpath(basedir,mydir,"forward_samestart"))
        mkdir(joinpath(basedir,mydir,"forward_samestart"))
    end

    cd(joinpath(basedir,mydir,"forward_samestart"))

    if !isdir(welldir)
        mkdir(welldir)
    end

    cd(joinpath(basedir,mydir,"forward_samestart",welldir))
    cp(joinpath("../..",yamlfile),fname,remove_destination=true)

    # Replace start time so that all forward runs have the same date range
    yamldata = YAML.load(open("./$(fname)"))
    pumpbegintime = yamldata["Pumping begin time"]
    # I HAD TO ADD ESCAPES TO THE QUOTES TO GET THIS TO WORK - AJ
    run(`find ./ -name $(fname) -exec sed -i "s/Pumping begin time: \'$(pumpbegintime)\'/Pumping begin time: \'$(newpumpbegintime)\'/g" {} \;`) 

    # Run setupmads_noBadDates
    cd(basedir)
    run(`pipenv run python setupmads_nobaddates2.py $(joinpath(mydir,"forward_samestart",yamlfile))`)
    madsfilename=joinpath(basedir,mydir,"forward_samestart",welldir,"$wellname.mads")
# ONLY ON SOME_______
#need to edit:
#    run(`fixmads.sh $madsfilename`)
    # Replace initial conditions with calibrated values
    # NEED TO SELECT RIGHT RERUN FILE? set up if statement to grab final one
    #
    #
    mdopt = Mads.loadmadsfile(joinpath(mydir,welldir,"$wellname-rerun-rerun.mads"))
    params = Mads.getparamsinit(mdopt)
    keys = Mads.getparamkeys(mdopt)
    mdbd = Mads.loadmadsfile(joinpath(mydir,"forward_samestart",welldir,"$wellname.mads")) # Use old parameters as search criteria
    params_init = Mads.getparamsinit(mdbd)

    cd(joinpath(basedir,mydir,"forward_samestart",welldir))
    # Replace floats
    for i in 1:length(keys)
        run(`find ./ -name $wellname.mads -exec sed -i "s/- $(keys[i]): {init: $(params_init[i]),/- $(keys[i]): {init: $(params[i]),/g" {} \;`)    
    end

    # Replace integers
    for i in 1:length(keys)
        run(`find ./ -name $wellname.mads -exec sed -i "s/- $(keys[i]): {init: $(Int(round(params_init[i]))),/- $(keys[i]): {init: $(params[i]),/g" {} \;`)    
    end

    # Run mads forward
    cd(basedir)
    run(`mads16 $(joinpath(mydir,"forward_samestart",welldir,"$wellname.mads")) forward`)

    # Make plot! (from main directory)
    run(`pipenv run python plot_theis_driver.py $(joinpath(mydir,"forward_samestart",welldir,"$wellname.yaml"))`)

    cd(basedir)
end

if length(ARGS) < 2 
    println("Arguments must be of the form mydir date")
    println("e.g. julia forward_cal_modpumpstart.jl chromium_2016 2016-01-01")
    quit()
end

mydir = ARGS[1]
newpumpbegintime = ARGS[2]
include(joinpath(mydir,"wellnames.jl"))
for yamlfile in yamlfiles
    try
        wellsforward_nobaddates_samestart(yamlfile)
        println("Success: $yamlfile")
    catch
    	println("Error: $yamlfile")
    end
end
