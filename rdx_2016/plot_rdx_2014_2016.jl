# Id: plot_waterLevel_rates.jl, Thu 22 Dec 2016 11:04:11 AM MST pandeys #
# Created by Sachin Pandey, LANL
# Description: Plot water levels and rates together
#------------------------------------------------------------------------------
using YAML
using DataFrames
using PyPlot
plt = PyPlot
import PyCall
@PyCall.pyimport aquiferdb as db
@PyCall.pyimport chipbeta as cb
@PyCall.pyimport wellspy as wp
@PyCall.pyimport waterlevelinfo as wl

basedir = "/lclscratch/sach/pumptests2016"
mydir = "rdx_2016"
mydir2 = "rdx_2014_2016"
savedir = "rdx_2016_report"

yamlfiles = [ 
            "CdV-16-4ip/CdV-16-4ip#1.yaml",
            "R-25/R-25#2.yaml",
            "R-25b/R-25b.yaml",
            ]

ax2_legend_pos = [3,1,1]

productionwells = ["CdV-16-4ip","CdV-16-1(i)","CdV-9-1(i)"]

if isdir(joinpath(basedir,savedir,"figures"))
else
    mkdir(joinpath(basedir,savedir,"figures"))
end 

if isdir(joinpath(basedir,savedir,"figures","appZ"))
else
    mkdir(joinpath(basedir,savedir,"figures","appZ"))
end 

for yamlfile in yamlfiles
yamlindex = find(yamlfiles .== yamlfile)[1]


@show yamlfile
try
#------------------------------------------------------------------------------
# First grab pumping rates
#------------------------------------------------------------------------------
mycmap = get_cmap("Paired",length(productionwells)+1)

# Grab pumping rates for all times
pumpbegintime = "2001-01-01"
pumpendtime = "2018-01-01"
db.connecttodb()
production = Dict()
for productionwell in productionwells
    i = find(productionwells .== productionwell)[1]
    production[productionwell] = Dict()
    production[productionwell]["rates"], production[productionwell]["times"] = db.getpumpingrates(productionwell, pumpbegintime, pumpendtime)
    production[productionwell]["x"], production[productionwell]["y"], production[productionwell]["r"] = db.getgeometry(productionwell)
end
db.disconnectfromdb()

#------------------------------------------------------------------------------
# First Plot: Raw data, all times, with pumping
#------------------------------------------------------------------------------
# Initialize figure
fig, ax = plt.subplots(3,figsize=(8.125,10.5))

# Well name stuff
welldir =  split(yamlfile,"/")[1]
fname = split(yamlfile,"/")[2]
wellname = split(fname,".")[1]

# YAML stuff
yamldata = YAML.load(open(joinpath(basedir,mydir,"forward",welldir,fname)))
observationwell = yamldata["Observation well"]
observationbegintime = "2014-01-01"
endtime = "2018-01-01"

portdescr=[]
try
  portdescr = string(yamldata["Screen number"])
catch
  portdescr = "SINGLE COMPLETION"
end

# Data processing
db.connecttodb()
waterlevels, waterleveltimes = db.getwaterlevels(observationwell, portdescr, observationbegintime, endtime)
db.disconnectfromdb()

#------------------------------------------------------------------------------
# Second Plot: Cleaned up data, select times, with pumping
#------------------------------------------------------------------------------
dateform = Dates.DateFormat("y-m-d H:M:S") # to convert yaml dates to datetime
observationbegintime = yamldata["Observation begin time"]
endtime = yamldata["End time"]

baddates = []
try
  baddates = yamldata["Bad dates"]
catch
end

jumpfixdates = []
jumpfixsize = []
try
    jumpfixdates = map((x) -> DateTime(x[1],dateform), yamldata["Observation jumps"])
    jumpfixsize  = map((x) -> x[2], yamldata["Observation jumps"])
catch
end

samplethreshold=[]
try
    samplethreshold = controldata["Sample threshold"]
catch
    samplethreshold = 0.08
end

samplelength=[]
try
    samplelength = controldata["Sample length"]
catch
    samplelength = 5
end

barometricbegintime = Dates.format(DateTime(observationbegintime,dateform)+Dates.Day(-1),"yyyy-mm-dd")
barometricendtime = Dates.format(DateTime(endtime,dateform)+Dates.Day(-1),"yyyy-mm-dd")

# remove the bad dates
for baddate in baddates    
    baddate_datetime = map((x) -> DateTime(x,dateform), hcat(baddate...))
    goodwaterlevels = []
    goodwaterleveltimes = []
  for j in 1:length(waterlevels)
    if waterleveltimes[j] > baddate_datetime[1] && waterleveltimes[j] < baddate_datetime[2]
    else
      push!(goodwaterlevels,waterlevels[j])
      push!(goodwaterleveltimes,waterleveltimes[j])    
    end
  end
  waterlevels = goodwaterlevels
    waterleveltimes = goodwaterleveltimes
end

# fix the jumps in the water levels
j = 1
for jumpfixdate in jumpfixdates
  i = 1
  while i < length(waterleveltimes) && waterleveltimes[i] < jumpfixdate
    i += 1
  end
  while i < length(waterleveltimes)
    waterlevels[i] += jumpfixsize[j]
    i += 1
  end
  j += 1
end

# get the barometric information
db.connecttodb()
baropressmb, barotimes = db.getbarometricpressure(barometricbegintime, barometricendtime)

# get the earth tide information -- for now, just read it from a file
# TODO: 
# For some reason, db.getearthtide truncates the last date when called from julia
earthtideforces, earthtidetimes = db.getearthtide(barometricbegintime, barometricendtime)
db.disconnectfromdb()

waterlevelsnosamp, waterleveltimesnosamp = cb.removeSamplingEvents(waterlevels, waterleveltimes, threshold=samplethreshold, samplinglength=samplelength)
nosampwaterlevels, nosampwaterleveltimes = cb.denoise(waterlevelsnosamp, waterleveltimesnosamp, baropressmb, barotimes, earthtideforces, earthtidetimes)
waterlevels = nosampwaterlevels
waterleveltimes = nosampwaterleveltimes

# remove the bad dates from the no samp water levels
for baddate in baddates
    baddate_datetime = map((x) -> DateTime(x,dateform), hcat(baddate...))
    goodwaterlevels = []
    goodwaterleveltimes = []
    for j in 1:length(waterlevels)
        if waterleveltimes[j] > baddate_datetime[1] && waterleveltimes[j] < baddate_datetime[2]
        else
            push!(goodwaterlevels,waterlevels[j])
            push!(goodwaterleveltimes,waterleveltimes[j])    
        end
    end
    waterlevels = goodwaterlevels
    waterleveltimes = goodwaterleveltimes
end

#------------------------------------------------------------------------------
# Calibration results, total drawdown 2014/2016
#------------------------------------------------------------------------------
spoint = wp.SPoint(joinpath(basedir,mydir2,"forward",welldir,"$(wellname).s_point"))
spwellname = spoint[:getwellname]()
spwaterleveltimes = spoint[:getwaterleveltimes]()
spwaterlevels = spoint[:getwaterlevels]()
ddnames = spoint[:getdrawdownnames]()
drawdowns = spoint[:getdrawdowns]()
waterlevelinfo = wl.waterlevelinfo(joinpath(basedir,mydir2,yamlfile))

ax[1][:plot](waterlevelinfo[spwellname]["times"], waterlevelinfo[spwellname]["waterlevels"], "k.")
ax[1][:plot](spwaterleveltimes, spwaterlevels, "r", linewidth=2)


if observationwell == "CdV-16-4ip-S1"
  ax[1][:set_title]("CdV-16-4ip screen 1\n(A)")
elseif portdescr == "SINGLE COMPLETION"
  ax[1][:set_title]("$(observationwell)\n(A)")
else
  ax[1][:set_title]("$(observationwell) screen $(portdescr)\n(A)")
end

# plot production information
ax2 = ax[1][:twinx]() # Create another axis on top of the current axis
mycmap = get_cmap("Paired",length(productionwells)+1)
for productionwell in productionwells
    i = find(productionwells .== productionwell)[1]

    df = DataFrame(times = production[productionwell]["times"],rates = production[productionwell]["rates"])
    ax2[:bar](df[:times],df[:rates]*264.172/60/24,label = productionwell,color=mycmap(i),align="center",edgecolor=mycmap(i))
end

ax[1][:set_xlim](minimum(spwaterleveltimes),maximum(spwaterleveltimes))
ax[1][:set_zorder](ax2[:get_zorder]()+1) # put ax in front of ax2 
ax[1][:patch][:set_visible](false) # hide the 'canvas' 

#------------------------------------------------------------------------------
# Calibration results, total drawdown 2016
#------------------------------------------------------------------------------
spoint = wp.SPoint(joinpath(basedir,mydir,"forward",welldir,"$(wellname).s_point"))
spwellname = spoint[:getwellname]()
spwaterleveltimes = spoint[:getwaterleveltimes]()
spwaterlevels = spoint[:getwaterlevels]()
ddnames = spoint[:getdrawdownnames]()
drawdowns = spoint[:getdrawdowns]()
waterlevelinfo = wl.waterlevelinfo(joinpath(basedir,mydir,yamlfile))

ax[2][:plot](waterlevelinfo[spwellname]["times"], waterlevelinfo[spwellname]["waterlevels"], "k.")
ax[2][:plot](spwaterleveltimes, spwaterlevels, "r", linewidth=2)
ax[2][:set_title]("(B)") # Create another axis on top of the current axis

# plot production information
ax3 = ax[2][:twinx]() # Create another axis on top of the current axis
mycmap = get_cmap("Paired",length(productionwells)+1)
for productionwell in productionwells
    i = find(productionwells .== productionwell)[1]

    df = DataFrame(times = production[productionwell]["times"],rates = production[productionwell]["rates"])
    ax3[:bar](df[:times],df[:rates]*264.172/60/24,label = productionwell,color=mycmap(i),align="center",edgecolor=mycmap(i))
end

ax[2][:set_xlim](minimum(spwaterleveltimes),maximum(spwaterleveltimes))
ax[2][:set_zorder](ax2[:get_zorder]()+1) # put ax in front of ax2 
ax[2][:patch][:set_visible](false) # hide the 'canvas' 

handles, labels = ax3[:get_legend_handles_labels]()
handles = [handles[3]  ,handles[2],handles[1]]
labels =  ["CdV-9-1i","CdV-16-1i" ,"CdV-16-4ip"]
ax3[:legend](handles,labels,loc=ax2_legend_pos[yamlindex],fontsize=11)

#------------------------------------------------------------------------------
# Calibration results, individual drawdown 2016
#------------------------------------------------------------------------------
mycmap2 = get_cmap("gist_ncar",length(ddnames)+1)
ddnames_labels = map((ddname) -> replace(ddname,"0",""), ddnames) # remove 0 from well names
mod_dd = [] # used to recalculate total

for i in 1:length(ddnames)
    mindd = minimum(drawdowns[ddnames[i]])        
    if ddnames[i] == "trend"
        ax[3][:plot](spwaterleveltimes, map((dd) -> dd - mindd, drawdowns[ddnames[i]]), label = "trend", c="k", ls="--", linewidth=1.5)
        # append!(mod_dd, (map((dd) -> dd - mindd, drawdowns[ddnames[i]])))
        mod_dd = (map((dd) -> dd - mindd, drawdowns[ddnames[i]]))
    end
end

# Then plot wells
ctr_col = 1 # iterator for custom colormap
for i in 1:length(ddnames)
    mindd = minimum(drawdowns[ddnames[i]])
    if ddnames[i] != "total" && ddnames[i] != "trend"
        # append!(mod_dd, (map((dd) -> dd - mindd, drawdowns[ddnames[i]])))
        mod_dd = [mod_dd (map((dd) -> dd - mindd, drawdowns[ddnames[i]]))]
        ax[3][:plot](spwaterleveltimes, map((dd) -> dd - mindd, drawdowns[ddnames[i]]), c=mycmap2(ctr_col), label = ddnames_labels[i], linewidth=1.5)
        ctr_col += 1
    end
end

# Then plot total
# PROBLEM: Sometimes individual drawdowns are greater than total!
# SOLUTION 1: calculate a new total, which is done in the for loop above (mod_dd) 
total_dd = []
for i in 1:length(mod_dd[:,1])
    append!(total_dd,sum(mod_dd[i,:]))
end
ax[3][:set_xlim]([minimum(waterleveltimes),maximum(waterleveltimes)])
ax[3][:plot](spwaterleveltimes, total_dd, label = "total", c="r", linewidth=1.75)
ax[3][:invert_yaxis]()
ax[3][:set_title]("(C)")

#------------------------------------------------------------------------------
# Make plots pretty
#------------------------------------------------------------------------------
labels = ax[1][:get_xticklabels]() 
for label in labels
    label[:set_rotation](20) 
end

labels = ax[2][:get_xticklabels]() 
for label in labels
    label[:set_rotation](20) 
end

labels = ax[3][:get_xticklabels]() 
for label in labels
    label[:set_rotation](20) 
end

ax[1][:set_ylabel]("Water level (m)")
ax2[:set_ylabel]("Pumping rate, (gpm)")
ax[2][:set_ylabel]("Water level (m)")
ax3[:set_ylabel]("Pumping rate, (gpm)")
ax[3][:set_ylabel]("Drawdown (m)")

#ax[1][:set_xlim](DateTime("2016-01-01T00:00:00"),DateTime("2017-01-01T00:00:00"))
#ax[2][:set_xlim](DateTime("2016-01-01T00:00:00"),DateTime("2017-01-01T00:00:00"))

#ax[1][:set_xlim](DateTime("2016-05-01T00:00:00"),DateTime("2017-01-01T00:00:00"))
#ax[2][:set_xlim](DateTime("2016-05-01T00:00:00"),DateTime("2017-01-01T00:00:00"))

plt.tight_layout(pad=1.0,w_pad=1.0,h_pad=1.0)
#=
box = ax[1][:get_position]()
ax[1][:set_position]([box[:x0], box[:y0], box[:width] * 0.85, box[:height]])

box = ax[2][:get_position]()
ax[2][:set_position]([box[:x0], box[:y0], box[:width] * 0.85, box[:height]])

box = ax[3][:get_position]()
ax[3][:set_position]([box[:x0], box[:y0], box[:width] * 0.85, box[:height]])
=#

handles, labels = ax[3][:get_legend_handles_labels]()
# sort both labels and handles by labels
#labels, handles = zip(*sorted(zip(labels, handles), key=lambda t: t[0]))

if wellname == "R-63"
  handles = [handles[6]  ,handles[2],handles[4]  ,handles[3],handles[5],handles[1],handles[7]]
  labels =  ["CdV-9-1i","CdV-16-1i" ,"CdV-16-4ip",labels[3] ,labels[5] ,labels[1] , labels[7]]
else
  handles = [handles[4]  ,handles[2],handles[3]  ,handles[1],handles[5]]
  labels =  ["CdV-9-1i","CdV-16-1i" ,"CdV-16-4ip",labels[1] ,labels[5]]
end

#ax[3][:legend](handles,labels,loc=4, bbox_to_anchor=(1.29, 0.0),fontsize=11)
ax[3][:legend](handles,labels,loc=3,fontsize=11)

plt.savefig(joinpath(basedir,savedir,"figures","appZ","$(wellname)_2014_2016.png"),dpi=600)
plt.close()

catch
println("problems with $yamlfile")
end

end
