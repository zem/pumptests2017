# Id: plot_waterLevel_rates.jl, Thu 22 Dec 2016 11:04:11 AM MST pandeys #
# Created by Sachin Pandey, LANL
# Description: Plot water levels and rates together
#------------------------------------------------------------------------------
using YAML
using DataFrames
using PyPlot
plt = PyPlot
import PyCall
@PyCall.pyimport aquiferdb as db
@PyCall.pyimport chipbeta as cb
@PyCall.pyimport wellspy as wp
@PyCall.pyimport waterlevelinfo as wl

basedir = "/lclscratch/sach/pumptests2016"
mydir = "rdx_2016_report"

yamlfile = "CdV-9-1i/CdV-9-1i#1.yaml"
ax2_legend_pos = [1]
productionwells = ["CdV-16-4ip","CdV-16-1(i)","CdV-9-1(i)"]

if isdir(joinpath(basedir,mydir,"figures"))
else
    mkdir(joinpath(basedir,mydir,"figures"))
end 

if isdir(joinpath(basedir,mydir,"figures","appZ"))
else
    mkdir(joinpath(basedir,mydir,"figures","appZ"))
end 

yamlindex = 1
@show yamlfile
#------------------------------------------------------------------------------
# First grab pumping rates
#------------------------------------------------------------------------------
mycmap = get_cmap("Paired",length(productionwells)+1)

# Grab pumping rates for all times
pumpbegintime = "2001-01-01"
pumpendtime = "2018-01-01"
db.connecttodb()
production = Dict()
for productionwell in productionwells
    i = find(productionwells .== productionwell)[1]
    production[productionwell] = Dict()
    production[productionwell]["rates"], production[productionwell]["times"] = db.getpumpingrates(productionwell, pumpbegintime, pumpendtime)
    production[productionwell]["x"], production[productionwell]["y"], production[productionwell]["r"] = db.getgeometry(productionwell)
end
db.disconnectfromdb()

#------------------------------------------------------------------------------
# First Plot: Raw data, all times, with pumping
#------------------------------------------------------------------------------
# Initialize figure
fig, ax = plt.subplots(2,figsize=(8.125,7.0),sharex=true)
#fig, ax = plt.subplots(3,figsize=(8.125,10.5),sharex=true)

# Well name stuff
welldir =  split(yamlfile,"/")[1]
fname = split(yamlfile,"/")[2]
wellname = split(fname,".")[1]

# YAML stuff
yamldata = YAML.load(open(joinpath(basedir,mydir,"forward",welldir,fname)))
observationwell = yamldata["Observation well"]
observationbegintime = "2016-08-01"
endtime = "2017-01-01"

portdescr=[]
try
  portdescr = string(yamldata["Screen number"])
catch
  portdescr = "SINGLE COMPLETION"
end




#------------------------------------------------------------------------------
# Calibration results, total drawdown
#------------------------------------------------------------------------------
spoint = wp.SPoint(joinpath(basedir,mydir,"forward",welldir,"$(wellname).s_point"))
spwellname = spoint[:getwellname]()
spwaterleveltimes = spoint[:getwaterleveltimes]()
spwaterlevels = spoint[:getwaterlevels]()
ddnames = spoint[:getdrawdownnames]()
drawdowns = spoint[:getdrawdowns]()
waterlevelinfo = wl.waterlevelinfo(joinpath(basedir,mydir,yamlfile))

ax[1][:plot](waterlevelinfo[spwellname]["times"], waterlevelinfo[spwellname]["waterlevels"], "k.")
ax[1][:plot](spwaterleveltimes, spwaterlevels, "r", linewidth=2)

ax[1][:set_title]("CdV-9-1i screen $(portdescr)\n(A)")

# plot production information
ax2 = ax[1][:twinx]() # Create another axis on top of the current axis
mycmap = get_cmap("Paired",length(productionwells)+1)
for productionwell in productionwells
    i = find(productionwells .== productionwell)[1]

    df = DataFrame(times = production[productionwell]["times"],rates = production[productionwell]["rates"])
    ax2[:bar](df[:times],df[:rates]*264.172/60/24,label = productionwell,color=mycmap(i),align="center",edgecolor=mycmap(i))
end

ax[1][:set_xlim](minimum(spwaterleveltimes),maximum(spwaterleveltimes))
ax[1][:set_zorder](ax2[:get_zorder]()+1) # put ax in front of ax2 
ax[1][:patch][:set_visible](false) # hide the 'canvas' 
handles, labels = ax2[:get_legend_handles_labels]()
handles = [handles[3]  ,handles[2],handles[1]]
labels =  ["CdV-9-1i","CdV-16-1i" ,"CdV-16-4ip"]
ax2[:legend](handles,labels,loc=ax2_legend_pos[yamlindex],fontsize=11)


#------------------------------------------------------------------------------
# Calibration results, individual drawdown
#------------------------------------------------------------------------------
mycmap2 = get_cmap("gist_ncar",length(ddnames)+1)
ddnames_labels = map((ddname) -> replace(ddname,"0",""), ddnames) # remove 0 from well names
mod_dd = [] # used to recalculate total

for i in 1:length(ddnames)
    mindd = minimum(drawdowns[ddnames[i]])        
    if ddnames[i] == "trend"
        ax[2][:plot](spwaterleveltimes, map((dd) -> dd - mindd, drawdowns[ddnames[i]]), label = "trend", c="k", ls="--", linewidth=1.5)
        # append!(mod_dd, (map((dd) -> dd - mindd, drawdowns[ddnames[i]])))
        mod_dd = (map((dd) -> dd - mindd, drawdowns[ddnames[i]]))
    end
end

# Then plot wells
ctr_col = 1 # iterator for custom colormap
for i in 1:length(ddnames)
    mindd = minimum(drawdowns[ddnames[i]])
    if ddnames[i] != "total" && ddnames[i] != "trend"
        # append!(mod_dd, (map((dd) -> dd - mindd, drawdowns[ddnames[i]])))
        mod_dd = [mod_dd (map((dd) -> dd - mindd, drawdowns[ddnames[i]]))]
        ax[2][:plot](spwaterleveltimes, map((dd) -> dd - mindd, drawdowns[ddnames[i]]), c=mycmap2(ctr_col), label = ddnames_labels[i], linewidth=1.5)
        ctr_col += 1
    end
end

# Then plot total
# PROBLEM: Sometimes individual drawdowns are greater than total!
# SOLUTION 1: calculate a new total, which is done in the for loop above (mod_dd) 
total_dd = []
for i in 1:length(mod_dd[:,1])
    append!(total_dd,sum(mod_dd[i,:]))
end
ax[2][:set_xlim]([observationbegintime,endtime])
ax[2][:plot](spwaterleveltimes, total_dd, label = "total", c="r", linewidth=1.75)
ax[2][:invert_yaxis]()
ax[2][:set_title]("(B)")

#------------------------------------------------------------------------------
# Make plots pretty
#------------------------------------------------------------------------------
labels = ax[1][:get_xticklabels]() 
for label in labels
    label[:set_rotation](20) 
end

labels = ax[2][:get_xticklabels]() 
for label in labels
    label[:set_rotation](20) 
end

ax[1][:set_ylabel]("Water level (m)")
ax[2][:set_ylabel]("Drawdown (m)")
ax2[:set_ylabel]("Pumping rate (gpm)")

ax[2][:set_ylim](4,0)
ax[1][:set_ylim](2011,2014)
plt.tight_layout(pad=1.0,w_pad=1.0,h_pad=1.0)

handles, labels = ax[2][:get_legend_handles_labels]()
# sort both labels and handles by labels
#labels, handles = zip(*sorted(zip(labels, handles), key=lambda t: t[0]))


if wellname == "R-63"
  handles = [handles[6]  ,handles[2],handles[4]  ,handles[3],handles[5],handles[1],handles[7]]
  labels =  ["CdV-9-1i","CdV-16-1i" ,"CdV-16-4ip",labels[3] ,labels[5] ,labels[1] , labels[7]]
elseif wellname == "R-25#4-2014"
  handles = [handles[2]  ,handles[1],handles[3]]
  labels =  ["CdV-16-4ip",labels[1] ,labels[3]]
else
  handles = [handles[4]  ,handles[2],handles[3]  ,handles[1],handles[5]]
  labels =  ["CdV-9-1i","CdV-16-1i" ,"CdV-16-4ip",labels[1] ,labels[5]]
end

#ax[2][:legend](handles,labels,loc=4, bbox_to_anchor=(1.29, 0.0),fontsize=11)

if wellname == "R-25#4-2014"
  ax[2][:legend](loc=3, fontsize=11)
else
  ax[2][:legend](handles,labels,loc=3, fontsize=11)
end

plt.savefig(joinpath(basedir,mydir,"figures","appZ","$(wellname)_closeup.png"),dpi=600)
plt.close()
