import sachFun

basedir = "/lclscratch/sach/pumptests2016"
mydir = "rdx_2014_2016"
begintime = "2016-06-01"

yamlfiles = [ 
            "CdV-16-4ip/CdV-16-4ip#1.yaml",
            "R-25/R-25#2.yaml",
            "R-25b/R-25b.yaml",
            ]

if isdir(joinpath(basedir,mydir,"waterlevels_raw"))
else
    mkdir(joinpath(basedir,mydir,"waterlevels_raw"))
end 

for yamlfile in yamlfiles
    fname = split(yamlfile,"/")[end]
    wellname = split(fname,".")[1]
    welldir =  replace(yamlfile,fname,"")
    
    waterleveltimes,waterlevels = sachFun.rawWaterLevels(joinpath(basedir,mydir),yamlfile)
    outfile = open("./waterlevels_raw/waterlevels_$(wellname)_2014_2016.csv", "w")
    for i in 1:length(waterleveltimes)
        timestringarray = split(string(waterleveltimes[i]),"T")
        print(outfile, "$(timestringarray[1]) $(timestringarray[2]),")
        @printf(outfile, "%0.5e \n", waterlevels[i])
    end
    close(outfile)
end
