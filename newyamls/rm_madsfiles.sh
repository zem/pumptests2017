#!/bin/bash

find . -name "*.forward" -type f|xargs rm -f
find . -name "*wells_output" -type f|xargs rm -f
find . -name "*.tpl" -type f|xargs rm -f
find . -name "*.mads_output" -type f|xargs rm -f
find . -name "*.mads_output*" -type f|xargs rm -f
find . -name "*.cmdline_hist" -type f|xargs rm -f
find . -name "*.wells_debug" -type f|xargs rm -f
find . -name "*.wells" -type f|xargs rm -f
find . -name "*.s_point" -type f|xargs rm -f
find . -name "*.jacobian" -type f|xargs rm -f
find . -name "*.eigen" -type f|xargs rm -f
find . -name "*.phi" -type f|xargs rm -f
find . -name "*.ins" -type f|xargs rm -f
find . -name "*.results" -type f|xargs rm -f
find . -name "*.covariance" -type f|xargs rm -f
find . -name "*.correlation" -type f|xargs rm -f
find . -name "*.residuals" -type f|xargs rm -f
find . -name "*.mads" -type f|xargs rm -f
find . -name "*.running" -type f|xargs rm -f
find . -name "*.intermediate_residuals" -type f|xargs rm -f
find . -name "*.intermediate_results" -type f|xargs rm -f
find . -name "*.png" -type f|xargs rm -f
find . -name "*.pdf" -type f|xargs rm -f
