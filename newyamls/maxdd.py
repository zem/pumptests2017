# Id: maxdd.py, Thu 05 Jan 2017 03:38:38 PM MST pandeys #
# Created by Sachin Pandey, LANL
# Description: Calculate max drawdown (calculated for each production well) value and date 
#  - Note: This now gets data from forward runs with not bad dates.
#------------------------------------------------------------------------------
import wellspy
import datetime
import matplotlib
import matplotlib.pyplot as plt
import csv
from wellspy import SPoint
import numpy as np
import pandas as pd
import os

def crex1maxdd(obs_well_name,spointfilename):
    print('starting obs well '+obs_well_name)

    spoint = SPoint(spointfilename)
    spointwaterleveltimes = spoint.getwaterleveltimes()
    spointwaterlevels = spoint.getwaterlevels()

    ddnames = spoint.getdrawdownnames()
    drawdowns = spoint.getdrawdowns()
    
    mycmap=plt.cm.Paired(np.linspace(0,1,len(dd_well_names)))
    
    f,ax = plt.subplots(figsize=(15,7))
    for dd_well_name in dd_well_names:

        id_crex1 = [i for i,v in enumerate(ddnames) if v == dd_well_name]

        mindd = min(drawdowns[ddnames[id_crex1[0]]])

        dd_crex1 =  map(lambda dd: dd - mindd, drawdowns[ddnames[id_crex1[0]]])

        #plt.plot(spointwaterleveltimes,dd_crex1)

        # Maximum d.d. that occurred between the times ~11/01/15 - 01/01/16 as estimated by the theis model.
        date_min = datetime.datetime.strptime('2001-01-01', '%Y-%m-%d')
        date_max = datetime.datetime.strptime('2018-01-01', '%Y-%m-%d')

        idx = [i for i,v in enumerate(spointwaterleveltimes) if v > date_min]

        newtimes = [spointwaterleveltimes[i] for i in idx]
        newwaterlevels = [dd_crex1[i] for i in idx]

        idx2 = [i for i,v in enumerate(newtimes) if v < date_max]

        newtimes2 = [newtimes[i] for i in idx2]
        newwaterlevels2 = [newwaterlevels[i] for i in idx2]

        # simple for loop
        results = 0
        maxlevel = max(newwaterlevels2)
        for i in range(0,len(newwaterlevels2)):
          if newwaterlevels2[i] == maxlevel:
            results = i
        maxtime = newtimes2[results]
        print dd_well_name,maxtime,maxlevel," m"
        
        df_values.loc[obs_well_name,dd_well_name]=maxlevel
        
        maxtime_truncated = datetime.date(maxtime.year, maxtime.month, maxtime.day)
        df_dates.loc[obs_well_name,dd_well_name]=maxtime_truncated
        #print(type(maxtime_truncated))

        newwaterlevels3=[i for i in newwaterlevels2]

        color_index = dd_well_names.index(dd_well_name)
        plt.plot(newtimes2, newwaterlevels3, c=mycmap[color_index], linewidth=2, label=dd_well_name )


    plt.gca().invert_yaxis()
    plt.legend(loc=4,title='')
    #plt.ylabel("Drawdown [ft]")
    plt.ylabel("Drawdown [m]")
    plt.xlabel("Date")
    plt.xticks(rotation=30)

    # shrink axis
    plt.tight_layout()
    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width * 0.9, box.height])

    # sort both labels and handles by labels
    # Put a legend to the right of the current axis
    handles, labels = ax.get_legend_handles_labels()
    labels, handles = zip(*sorted(zip(labels, handles), key=lambda t: t[0]))
    ax.legend(handles, labels, loc="center left", bbox_to_anchor=(1, 0.5))

    f.savefig('./figures/maxdd/dd-'+obs_well_name+'-2016.png')
    plt.close('all')

date0 = datetime.datetime(2000, 1, 1)

if not os.path.exists('./figures/maxdd'):
    os.makedirs('./figures/maxdd')

yamlfiles = [ 
            "CrEX-1/CrEX-1#1.yaml",
            "CrEX-3/CrEX-3.yaml",
            "CrPZ-1/CrPZ-1.yaml",
            "CrPZ-2/CrPZ-2#1.yaml",
            "CrPZ-3/CrPZ-3.yaml",
            "CrPZ-4/CrPZ-4.yaml",
            "CrPZ-5/CrPZ-5.yaml",
            "R-01/R-01.yaml",
            "R-11/R-11.yaml",
            "R-13/R-13.yaml",
            "R-15/R-15.yaml",
            "R-28/R-28.yaml",
            "R-33/R-33#1.yaml",
            "R-33/R-33#2.yaml",
            "R-35a/R-35a.yaml",
            "R-35b/R-35b.yaml",
            "R-36/R-36.yaml",
            "R-42/R-42.yaml",
            "R-43/R-43#1.yaml",
            "R-43/R-43#2.yaml",    
            "R-44/R-44#1.yaml",
            "R-44/R-44#2.yaml",
            "R-45/R-45#1.yaml",
            "R-45/R-45#2.yaml",
            "R-50/R-50#1.yaml",
            "R-50/R-50#2.yaml",
            "R-61/R-61#1.yaml",
            "R-61/R-61#2.yaml",
            "R-62/R-62.yaml",
            "SIMR-2/SIMR-2.yaml",
            ]

dd_well_names = ['PM-01','PM-02','PM-03', 'PM-04', 'PM-05', 'O-04', 'CrEX-1', 'CrEX-2', 'CrEX-3', 'CrEX-4', 'R-28', 'R-42', 'CrIN-1', 'CrIN-2', 'CrIN-3', 'CrIN-4', 'CrIN-5', 'CrIN-6']
df_values = pd.DataFrame(columns=dd_well_names,index=map(lambda yamlfile : yamlfile.split("/")[1].split(".")[0],yamlfiles))
df_dates = pd.DataFrame(columns=dd_well_names,index=map(lambda yamlfile : yamlfile.split("/")[1].split(".")[0],yamlfiles))

for i in range(0,len(yamlfiles)):
    spointfilename = "./forward_samestart/" + yamlfiles[i].split("/")[0] + "/" + yamlfiles[i].split("/")[1].split(".")[0] + ".s_point"
    crex1maxdd(yamlfiles[i].split("/")[1].split(".")[0],spointfilename)

# Change index names as needed
baddnames = ["CrPZ-1-fixjumpmajor","CrPZ-2#1-nofixjumps","CrPZ-5-nofixjumps","R-62-bigskip"]
goodnames = ["CrPZ-1 corrected","CrPZ-2#1","CrPZ-5","R-62"]
obs_well_names = map(lambda yamlfile : yamlfile.split("/")[1].split(".")[0],yamlfiles)

for i in range(0,len(obs_well_names)):
  for j in range(0,len(baddnames)):
    if obs_well_names[i].find(baddnames[j])==0:
      obs_well_names[i]=goodnames[j]

df_values.index = obs_well_names
df_dates.index = obs_well_names

# Write to excel
writer = pd.ExcelWriter('./maxdd.xlsx', engine='xlsxwriter')
df_values.to_excel(writer,'maxdd_value')
df_dates.to_excel(writer,'maxdd_dates')

#workbook  = writer.book
worksheet = writer.sheets['maxdd_value']
worksheet.set_column('A:N', 12)
worksheet2 = writer.sheets['maxdd_dates']
worksheet2.set_column('A:N', 12)
writer.save()

# Plot max dd values as a heatmap
df_values = df_values.convert_objects(convert_numeric=True)

# remove dd caused by obs production
for i in range(0,len(yamlfiles)):
  for dd_well_name in dd_well_names:
    if yamlfiles[i].split("/")[1].split(".")[0].find(dd_well_name)==0:
      df_values.loc[yamlfiles[i].split("/")[1].split(".")[0],dd_well_name]=float('nan')

heatmap,ax = plt.subplots(figsize=(20,12))

df_valuesm = np.ma.masked_invalid(df_values)

plt.pcolor(df_valuesm.T)
plt.xticks(np.arange(0.5, len(df_values.index), 1), df_values.index)
plt.yticks(np.arange(0.5, len(df_values.columns), 1), df_values.columns)

plt.xlabel("Observation wells")
plt.ylabel("Production wells")
plt.title("Maximum contribution to total drawdown in 2016")

labels = ax.get_xticklabels()
for label in labels:
  label.set_rotation(-90)

plt.axis('tight')
plt.tight_layout()

plt.colorbar()

# heatmap.show()

heatmap.savefig('./maxdd.png')

plt.close('all')
