# Id: calc_rsquared.jl, Fri 02 Feb 2018 12:20:57 PM MST pandeys #
# Created by Sachin Pandey, LANL
# Description: Calculate r-squared for each Theis analysis.
#------------------------------------------------------------------------------
using YAML
using PyPlot
plt = PyPlot
import PyCall
@PyCall.pyimport wellspy as wp
@PyCall.pyimport waterlevelinfo as wl
import DataFrames
df = DataFrames
import JLD

function calc_rsquared(observed,modeled)
    ybar = mean(observed)
    SStot = sum(map(x->(x-ybar)^2,observed))
    SSres = sum((observed-modeled).^2)
    rsquared = 1-SSres/SStot
    return rsquared, SSres
end

# for debug
# ARGS = ["chromium_2016"]

if length(ARGS) < 1 
    println("Arguments must be of the form mydir")
    println("e.g. julia calc_rsquared.jl chromium_2016")
    quit()
end

basedir = pwd()
mydir = ARGS[1]
include(joinpath(basedir,mydir,"wellnames.jl"))

rsquared = Array{Float64}(length(yamlfiles))
SSres = Array{Float64}(length(yamlfiles))
thedata = Dict()
for yamlfile in yamlfiles
    @show yamlfile
    yamlindex = find(yamlfiles .== yamlfile)[1]
    @show yamlindex

    # Well name stuff
    welldir =  split(yamlfile,"/")[1]
    fname = split(yamlfile,"/")[2]
    wellname = split(fname,".")[1]
    
    #------------------------------------------------------------------------------
    # Calibration results, total drawdown
    #------------------------------------------------------------------------------
    spoint = wp.SPoint(joinpath(basedir,mydir,"forward",welldir,"$(wellname).s_point"))
    spwellname = spoint[:getwellname]()
    spwaterleveltimes = spoint[:getwaterleveltimes]()
    spwaterlevels = spoint[:getwaterlevels]()
    ddnames = spoint[:getdrawdownnames]()
    drawdowns = spoint[:getdrawdowns]()
    waterlevelinfo = wl.waterlevelinfo(joinpath(basedir,mydir,yamlfile))

    # Remove extra dates
    times_modeled = map(x->round(x, Dates.Hour),spwaterleveltimes)
    times_obs = waterlevelinfo[spwellname]["times"]
    times_modeled_filtered = map(x->times_modeled[times_modeled.==x],times_obs)
    waterlevels_modeled_filtered = map(x->spwaterlevels[times_modeled.==x],times_obs)

    times_modeled_filteredx = Array{DateTime}(length(times_modeled_filtered))
    waterlevels_modeled_filteredx =  Array{Float64}(length(times_modeled_filtered))
    for i in 1:length(times_modeled_filtered)
        if length(times_modeled_filtered[i]) > 1
            warn("Time overlap")    
        end
        times_modeled_filteredx[i]=times_modeled_filtered[i][1]
        waterlevels_modeled_filteredx[i]=waterlevels_modeled_filtered[i][1]
    end

    rsquared[yamlindex], SSres[yamlindex] = calc_rsquared(waterlevelinfo[spwellname]["waterlevels"],waterlevels_modeled_filteredx)
    thedata[wellname] = Dict()
    thedata[wellname]["times_obs"] = waterlevelinfo[spwellname]["times"]
    thedata[wellname]["times_modeled"] = times_modeled_filteredx
    thedata[wellname]["waterlevels_obs"] = waterlevelinfo[spwellname]["waterlevels"]
    thedata[wellname]["waterlevels_modeled"] = waterlevels_modeled_filteredx
end
results = df.DataFrame(well = map(x -> split(split(x,"/")[end],".")[1],yamlfiles), rsquared = rsquared, ssres = SSres)
df.writetable(joinpath(basedir,mydir,"rsquared.csv"), results)
JLD.save(joinpath(basedir,mydir,"rsquared.jld"),"dictionary",thedata)

#------------------------------------------------------------------------------
# Plot Observation dataRaw data
#------------------------------------------------------------------------------
# fig, ax = plt.subplots(5,6,figsize=(15.0,12.0))
# 
# for yamlfile in yamlfiles
#     @show yamlfile
#     yamlindex = find(yamlfiles .== yamlfile)[1]
#     @show yamlindex
# 
#     # Well name stuff
#     welldir =  split(yamlfile,"/")[1]
#     fname = split(yamlfile,"/")[2]
#     wellname = split(fname,".")[1]
#     
#     # YAML stuff
#     yamldata = YAML.load(open(joinpath(basedir,mydir,"forward",welldir,fname)))
#     observationwell = yamldata["Observation well"]
# 
#     portdescr=[]
#     try
#       portdescr = string(yamldata["Screen number"])
#     catch
#       portdescr = "SINGLE COMPLETION"
#     end
# 
#     ax[yamlindex][:plot](thedata[wellname]["times_obs"], thedata[wellname]["waterlevels_obs"], ls=" ", color = "0.45", marker = ".")
#     ax[yamlindex][:plot](thedata[wellname]["times_modeled"], thedata[wellname]["waterlevels_modeled"], "k", linewidth=2, label="total")
# 
#     if portdescr == "SINGLE COMPLETION"
#       ax[yamlindex][:set_title]("$(observationwell)\n(A)")
#     else
#       ax[yamlindex][:set_title]("$(observationwell)#$(portdescr)\n(A)")
#     end
# end
# plt.tight_layout()
