# Id: plot_params.jl, Wed 07 Feb 2018 05:20:35 PM MST pandeys #
# Created by Sachin Pandey, LANL
# Description: Use CrPlots to make spatial plot of parameters.
#------------------------------------------------------------------------------
import CrPlots
import Kriging
import PyCall
@PyCall.pyimport yaml

pumpingwells = ["O-04", "CrIN-3", "CrIN-2", "PM-05", "PM-04", "PM-02", "CrIN-5", "CrIN-4", "CrEX-1", "R-28", "CrEX-3", "R-42", "CrIN-1"]
paramfilenames = filter(fn->!endswith(fn, "#2.params"), filter(fn->endswith(fn, ".params"), map(chomp, readlines(`bash -c 'ls -1 */*.params'`))))#get only the top screens
wellnames = map(x->split(split(split(x, ".")[1], "#")[1], "/")[end], paramfilenames)
wellnames = map(x->x == "R-1" ? "R-01" : x, wellnames)
paramvalues = Dict()
for i = 1:length(paramfilenames)
	f = open(paramfilenames[i])
	paramvalues[wellnames[i]] = yaml.load(f)
	close(f)
end

welllocations = map(x->CrPlots.getwelllocation(x), wellnames)
x0 = minimum(map(x->x[1], welllocations)) - 250
x1 = maximum(map(x->x[1], welllocations)) + 250
y0 = minimum(map(x->x[2], welllocations)) - 250
y1 = maximum(map(x->x[2], welllocations)) + 250
boundingbox = (x0, y0, x1, y1)

fieldsdir = "figures/fieldplots"
if !isdir(fieldsdir) && myid() == 1
	mkdir(fieldsdir)
end

upperlimit = Dict()
lowerlimit = Dict()
upperlimit["K"] = 6
lowerlimit["K"] = 1
upperlimit["S"] = -0.3
lowerlimit["S"] = -3

for paramname in ["K", "S"]
	for plotwell in pumpingwells
		xs = Float64[]
		ys = Float64[]
		paramvals = Float64[]
		drawwells = Vector{String}()
		fullparamname = string(paramname, "_", plotwell)
		for i = 1:length(wellnames)
			push!(xs, welllocations[i][1])
			push!(ys, welllocations[i][2])
			push!(paramvals, paramvalues[wellnames[i]][fullparamname])
			push!(drawwells, string(wellnames[i]))
		end
		fig, ax, img = CrPlots.crplot(boundingbox, xs, ys, paramvals, h->Kriging.expcov(h, 1., 100), upperlimit=upperlimit[paramname], lowerlimit=lowerlimit[paramname])
		CrPlots.addcbar(fig, img, fullparamname, collect(linspace(lowerlimit[paramname], upperlimit[paramname], 5)),lowerlimit[paramname],upperlimit[paramname])
		
		CrPlots.addwells(ax, drawwells)
		if true
			display(fig)
			println()
		end
		fig[:savefig]("$fieldsdir/$(plotwell)_$(paramname).png")
		PyPlot.close(fig)
	end
end
