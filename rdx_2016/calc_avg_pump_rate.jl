using sachFun
sf = sachFun
using PyPlot
plt = PyPlot

productionwells = ["CDV-16-4ip","CdV-16-1(i)","CDV-9-1(i)"]
production = sf.dbPumpingRates(productionwells,"2016-01-01","2017-01-01")

fig,ax = subplots()
ax[:set_title]("LA County Pumping Rates")

mycmap = get_cmap("Paired",length(productionwells)+1)
for productionwell in productionwells
    i = find(productionwells .== productionwell)[1]
    production[productionwell]["rates"] = production[productionwell]["rates"]/60/24*264.172
    ax[:plot](production[productionwell]["times"],production[productionwell]["rates"],label = productionwell,color=mycmap(i),marker="o")
    rates = production[productionwell]["rates"]
    println("$productionwell mean rate = $(mean(rates[rates.>0.0]))")
end
