# Id: plot_waterLevel_rates.jl, Thu 22 Dec 2016 11:04:11 AM MST pandeys #
# Created by Sachin Pandey, LANL
# Description: Plot water levels and rates together
#------------------------------------------------------------------------------
import DataFrames
df = DataFrames

include("functions.jl")

if length(ARGS) < 1
    println("Arguments must be of the form mydir")
    println("e.g. julia plot_production_waterlevels.jl chromium_2017")
    quit()
end

mydir = ARGS[1]
include(joinpath(mydir,"wellnames.jl"))

if isdir(joinpath(mydir,"figures"))
else
    mkdir(joinpath(mydir,"figures"))
end 

if isdir(joinpath(mydir,"figures","production_waterlevels"))
else
    mkdir(joinpath(mydir,"figures","production_waterlevels"))
end 

for yamlfile in yamlfiles
    try
        welldir =  split(yamlfile,"/")[1]
        fname = split(yamlfile,"/")[2]
        wellname = split(fname,".")[1]

        #------------------------------------------------------------------------------
        # Water Levels
        #------------------------------------------------------------------------------
        waterleveltimes,waterlevels = plotWaterLevels(yamlfile,mydir)

        fig, ax = plt.subplots(figsize = (17,8))
        ax[:plot](waterleveltimes,waterlevels,"k.")    

		#------------------------------------------------------------------------------
		# Pumping
		#------------------------------------------------------------------------------
		# Don't change these times
		pumpbegintime = "2001-01-01"
		pumpendtime = "2050-01-01"

		mycmap = get_cmap("Paired",length(productionwells)+1)

		# Grab pumping rates for all times
		db.connecttodb()
		production = Dict()
		for productionwell in productionwells
			i = find(productionwells .== productionwell)[1]
			production[productionwell] = Dict()
			production[productionwell]["rates"], production[productionwell]["times"] = db.getpumpingrates(productionwell, pumpbegintime, pumpendtime)
			# production[productionwell]["x"], production[productionwell]["y"], production[productionwell]["r"] = db.getgeometry(productionwell)
		end
		db.disconnectfromdb()

		# Initialize figure
		ax2 = ax[:twinx]() # Create another axis on top of the current axis

		# Plot data
		for productionwell in productionwells
			i = find(productionwells .== productionwell)[1]
			if length(production[productionwell]["times"]) > 0
				results = df.DataFrame(times = production[productionwell]["times"],rates = production[productionwell]["rates"])
				newresults = df.DataFrame(times=DateTime[],rates=Float64[])

				for j in 1:length(results[:,1])-1
					push!(newresults,[results[j,:][:times][1],results[j,:][:rates][1]])
					push!(newresults,[results[j+1,:][:times][1],results[j,:][:rates][1]])
				end

				if results[end,:][:rates][1] == 0.0
					push!(newresults,[results[end,:][:times][1],results[end,:][:rates][1]])
				end

				if results[1,:][:rates][1] != 0.0
					results = append!(df.DataFrame(times=results[1,:][:times][1],rates=0.0),newresults)
				else
					results = newresults
				end

				ax2[:plot](results[:times],results[:rates]*264.172/60/24,label = productionwell,color=mycmap(i),linewidth=1.5)
			end
		end

        labels = ax[:get_xticklabels]()
        for label in labels
            label[:set_rotation](30) 
        end

        #ax[:set_xlim]([minimum(waterleveltimes),maximum(waterleveltimes)])
        ax[:set_xlim]([736584.0,maximum(waterleveltimes)])
        ax[:set_title](wellname)

        ax[:set_zorder](ax2[:get_zorder]()+1) # put ax in front of ax2
        ax[:patch][:set_visible](false) # hide the 'canvas'
        ax[:set_ylabel]("Water level, (m)")
        ax2[:set_ylabel]("Pumping rate, (m^3/day)")

        plt.tight_layout()
        # Shrink current axis
        box = ax[:get_position]()
        new_position = [box[:x0], box[:y0], box[:width] * 0.85, box[:height]]
        ax[:set_position](new_position)
        ax2[:set_position](new_position)

        # Put a legend to the right of the current axis
        ax2[:legend](loc="center left", bbox_to_anchor=(1.1, 0.5))
        
        plt.savefig(joinpath(mydir,"figures","production_waterlevels","production_waterlevels_$(wellname).png"))
        plt.close("all")
    catch
        println("problems with $yamlfile")
    end
end
