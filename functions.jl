# Id: plot_waterlevel.jl, Thu 20 Dec 2016 02:27:18 PM MDT pandeys #
# Created by Sachin Pandey, LANL
# Description: Plot water levels in Julia
# - This is a function to help identify observation starttime, endtime, gaps
#------------------------------------------------------------------------------
import YAML
using PyPlot
plt = PyPlot
import PyCall
@PyCall.pyimport aquiferdb as db
@PyCall.pyimport chipbeta as cb

function plotWaterLevels(yamlfile,mydir)
	# Visualize water level data during intermediate preprocessing stages
	dateform = Dates.DateFormat("y-m-d H:M:S") # to convert yaml dates to datetime
	welldir = split(yamlfile,"/")[1]
	fname = split(yamlfile,"/")[2]
	wellname = split(fname,".")[1]

	println("starting well $wellname")
	
	if isdir(joinpath(mydir,"figures"))
	else
		mkdir(joinpath(mydir,"figures"))
	end 

	if isdir(joinpath(mydir,"figures","waterlevels"))
	else
		mkdir(joinpath(mydir,"figures","waterlevels"))
	end 

	#------------------------------------------------------------------------------
	# Initialization
	#------------------------------------------------------------------------------
	yamldata = YAML.load(open(joinpath(mydir,welldir,fname)))
	observationwell = yamldata["Observation well"]
	observationbegintime = yamldata["Observation begin time"]
	endtime = yamldata["End time"]

	portdescr=[]
	try
		portdescr = string(yamldata["Screen number"])
	catch
		portdescr = "SINGLE COMPLETION"
	end

	jumpfixdates = []
	jumpfixsize = []
	try
		jumpfixdates = map((x) -> DateTime(x[1],dateform), yamldata["Observation jumps"])
		jumpfixsize = map((x) -> x[2], yamldata["Observation jumps"])
	catch
	end

	baddates = []
	try
		baddates = yamldata["Bad dates"]
	catch
	end

	samplethreshold=[]
	try
		samplethreshold = controldata["Sample threshold"]
	catch
		samplethreshold = 0.08
	end

	samplelength=[]
	try
		samplelength = controldata["Sample length"]
	catch
		samplelength = 5
	end

	#------------------------------------------------------------------------------
	# Data processing
	#------------------------------------------------------------------------------
	db.connecttodb()

	# raw water levels
	waterlevels, waterleveltimes = db.getwaterlevels(observationwell, portdescr, observationbegintime, endtime)
xmin=minimum(waterleveltimes)
#xmin=DateTime(2017, 1, 1)
#xmin=736384.0
#ymax=1778.2
#ymin=1777.6
ymin=minimum(waterlevels)
ymax=maximum(waterlevels)

#ymin=minimum(waterlevels)
#ymax=maximum(waterlevels)
	f, axarray = plt.subplots(5, 1, sharex=true, figsize = (10,15), dpi=300)
	axarray[1][:plot](waterleveltimes, waterlevels, "k.")
	axarray[1][:set_xlim]([xmin,maximum(waterleveltimes)])
	axarray[1][:set_ylim]([ymin,ymax])
	axarray[1][:set_title]("$wellname \n Raw Water Level Dataset:")
axarray[1][:set_ylabel]("Water level (m)")

array=zeros(Float64,length(waterleveltimes),3);
array[:,1]=datetime2julian.(waterleveltimes);
array[:,2]=datetime2julian.(waterleveltimes)-2415018.5;
array[:,3]=waterlevels;
csvfilename=joinpath(mydir,"csvfiles","waterlevels1_$(wellname).csv");
writedlm(csvfilename,array,',')

	# remove the bad dates
	for baddate in baddates
		baddate_datetime = map((x) -> DateTime(x,dateform), hcat(baddate...))
		goodwaterlevels = []
		goodwaterleveltimes = []
		for j in 1:length(waterlevels)
		if waterleveltimes[j] > baddate_datetime[1] && waterleveltimes[j] < baddate_datetime[2]
		else
			push!(goodwaterlevels,waterlevels[j])
			push!(goodwaterleveltimes,waterleveltimes[j])
		end
		end
		waterlevels = goodwaterlevels
		waterleveltimes = goodwaterleveltimes
	end

	axarray[2][:plot](waterleveltimes, waterlevels, "k.")
	axarray[2][:set_xlim]([xmin,maximum(waterleveltimes)])
	axarray[2][:set_ylim]([ymin,ymax])
	axarray[2][:set_title]("Dataset Excluding Corrupted Entries:")
axarray[2][:set_ylabel]("Water level (m)")
axarray[2][:plot]([DateTime(2010, 10, 22, 9, 30),DateTime(2010, 10, 22, 9, 30)], [0,10000],linewidth=1)
axarray[2][:plot]([DateTime(2012, 7, 24, 8, 30),DateTime(2012, 7, 24, 8, 30)], [0,10000],linewidth=1)
axarray[2][:plot]([DateTime(2013, 4, 24, 7, 30),DateTime(2013, 4, 24, 7, 30)], [0,10000],linewidth=1)

	# fix the jumps in the water levels
	j = 1
	for jumpfixdate in jumpfixdates
		i = 1
		while i < length(waterleveltimes) && waterleveltimes[i] < jumpfixdate
		i += 1
		end
		while i < length(waterleveltimes)
		waterlevels[i] += jumpfixsize[j]
		i += 1
		end
		j += 1
	end

	axarray[3][:plot](waterleveltimes, waterlevels, "k.")
	axarray[3][:set_xlim]([xmin,maximum(waterleveltimes)])
	axarray[3][:set_ylim]([ymin,ymax])
	axarray[3][:set_title]("Dataset Accounting for Transducer Slips:")
axarray[3][:plot]([DateTime(2010, 10, 22, 9, 30),DateTime(2010, 10, 22, 9, 30)], [0,10000],linewidth=1)
axarray[3][:plot]([DateTime(2012, 7, 24, 8, 30),DateTime(2012, 7, 24, 8, 30)], [0,10000],linewidth=1)
axarray[3][:plot]([DateTime(2013, 4, 24, 7, 30),DateTime(2013, 4, 24, 7, 30)], [0,10000],linewidth=1)

axarray[3][:set_ylabel]("Water level (m)")
	# get the barometric information
	barometricbegintime = Dates.format(DateTime(observationbegintime,dateform)+Dates.Day(-1),"yyyy-mm-dd")
	barometricendtime = Dates.format(DateTime(endtime,dateform)+Dates.Day(-1),"yyyy-mm-dd")
	baropressmb, barotimes = db.getbarometricpressure(barometricbegintime, barometricendtime)

	# get the earth tide information -- for now, just read it from a file
	earthtideforces, earthtidetimes = db.getearthtide(barometricbegintime, barometricendtime)

	waterlevelsnosamp, waterleveltimesnosamp = cb.removeSamplingEvents(waterlevels, waterleveltimes, threshold=samplethreshold, samplinglength=samplelength)
	nosampwaterlevels, nosampwaterleveltimes = cb.denoise(waterlevelsnosamp, waterleveltimesnosamp, baropressmb, barotimes, earthtideforces, earthtidetimes)
	waterlevels = nosampwaterlevels
	waterleveltimes = nosampwaterleveltimes

	axarray[4][:plot](waterleveltimes, waterlevels, "k.")
	axarray[4][:set_xlim]([xmin,maximum(waterleveltimes)])
	axarray[4][:set_ylim]([ymin,ymax])
	axarray[4][:set_title]("Dataset with Barometric Pressure/Earth Tide Corrections and Denoising:")
axarray[4][:set_ylabel]("Water level (m)")

println(length(waterleveltimes))
println(length(barotimes))

array=zeros(Float64,length(waterleveltimes),3);
array[:,1]=datetime2julian.(waterleveltimes);
array[:,2]=datetime2julian.(waterleveltimes)-2415018.5;
array[:,3]=waterlevels;
csvfilename=joinpath(mydir,"csvfiles","waterlevels4_$(wellname).csv");
writedlm(csvfilename,array,',')

#array=zeros(Float64,length(barotimes),3);
#array[:,1]=datetime2julian.(barotimes);
#array[:,2]=datetime2julian.(barotimes)-2415018.5;
#array[:,3]=baropressmb;
#csvfilename=joinpath(mydir,"csvfiles","baro4_$(wellname).csv");
#writedlm(csvfilename,array,',')

	# remove the bad dates from the no samp water levels
	for baddate in baddates
		baddate_datetime = map((x) -> DateTime(x,dateform), hcat(baddate...))
		goodwaterlevels = []
		goodwaterleveltimes = []
		for j in 1:length(waterlevels)
			if waterleveltimes[j] > baddate_datetime[1] && waterleveltimes[j] < baddate_datetime[2]
			else
				push!(goodwaterlevels,waterlevels[j])
				push!(goodwaterleveltimes,waterleveltimes[j])
			end
		end
		waterlevels = goodwaterlevels
		waterleveltimes = goodwaterleveltimes
	end
array=zeros(Float64,length(waterleveltimes),3);
array[:,1]=datetime2julian.(waterleveltimes);
array[:,2]=datetime2julian.(waterleveltimes)-2415018.5;
array[:,3]=waterlevels;
csvfilename=joinpath(mydir,"csvfiles","waterlevels5_$(wellname).csv");
writedlm(csvfilename,array,',')

	axarray[5][:plot](waterleveltimes, waterlevels, "k.")
	axarray[5][:set_xlim]([xmin,maximum(waterleveltimes)])
	axarray[5][:set_ylim]([ymin,ymax])
	axarray[5][:set_title]("Dataset for Theis Analysis:")
	axarray[5][:set_ylabel]("Water level (m)")
	labels = axarray[5][:get_xticklabels]()
	for label in labels
		label[:set_rotation](30) 
	end
	db.disconnectfromdb()

	plt.savefig(joinpath(mydir,"figures","waterlevels","waterlevels_$(wellname).png"))
	plt.close("all")

	# @show minimum(waterleveltimes)
	# @show maximum(waterleveltimes)
	return waterleveltimes,waterlevels
end

function waterLevelBadDates(yamlfile;timedelta = 5.0)
	# Go through water level data and find bad dates (i.e. large skips in data)
	# Must be up one directory above, e.g:
	# waterLevelBadDates("R-63i/R-63i.yaml", timedelta = 5)
	# time delta [days]: look for gaps bigger than time delta
	fname = split(yamlfile,"/")[end]
	wellname = split(fname,".")[1]
	welldir = replace(yamlfile,fname,"")

	println("finding gaps in water levels for $wellname")
	yamldata = YAML.load(open(joinpath(welldir,fname)))
	observationwell = yamldata["Observation well"]
	observationbegintime = yamldata["Observation begin time"]
	endtime = yamldata["End time"]

	portdescr=[]
	try
		portdescr = string(yamldata["Screen number"])
	catch
		portdescr = "SINGLE COMPLETION"
	end

	db.connecttodb()
	waterlevels, waterleveltimes = db.getwaterlevels(observationwell, portdescr, observationbegintime, endtime)
	db.disconnectfromdb()

	try
		newbaddates = yamldata["Bad dates"]
	catch
		newbaddates = Array{Array{String,1},1}()
	end
	gaplimit = Base.Dates.Millisecond(Base.Dates.Day(timedelta))
	gapstart = []
	gapend = []
	println("Bad dates:")
	for i in 2:length(waterleveltimes)
		temp = Array{String,1}()
		timedelta = waterleveltimes[i]-waterleveltimes[i-1]
		if timedelta > gaplimit
			gapstart = Dates.format(floor(waterleveltimes[i-1], Dates.Day), "yyyy-mm-dd HH:MM:SS")
			gapend = Dates.format(ceil(waterleveltimes[i], Dates.Day), "yyyy-mm-dd HH:MM:SS")
			println("- ['$gapstart', '$gapend']")
			append!(temp,[gapstart])
			append!(temp,[gapend])
			push!(newbaddates,temp)
		end
	end
	newbaddates = unique(newbaddates)
	return newbaddates
end

function rawWaterLevels(yamlfile)
	# Get raw (unprocessed) waterlevels from mysql database using aquiferdb
	fname = split(yamlfile,"/")[end]
	wellname = split(fname,".")[1]
	welldir = replace(yamlfile,fname,"")

	println("getting raw water levels for $wellname")
	yamldata = YAML.load(open(joinpath(basedir,welldir,fname)))
	observationwell = yamldata["Observation well"]
	observationbegintime = yamldata["Observation begin time"]
	endtime = yamldata["End time"]

	portdescr=[]
	try
		portdescr = string(yamldata["Screen number"])
	catch
		portdescr = "SINGLE COMPLETION"
	end

	db.connecttodb()
	waterlevels, waterleveltimes = db.getwaterlevels(observationwell, portdescr, observationbegintime, endtime)
	db.disconnectfromdb()

	return waterleveltimes,waterlevels
end

function denoiseWaterLevels(yamlfile)
	# Conduct all stages of processing raw waterlevels and return results
	dateform = Dates.DateFormat("y-m-d H:M:S") # to convert yaml dates to datetime
	fname = split(yamlfile,"/")[end]
	wellname = split(fname,".")[1]
	welldir = replace(yamlfile,fname,"")

	println("getting water levels for $wellname")

	#------------------------------------------------------------------------------
	# Initialization
	#------------------------------------------------------------------------------
	yamldata = YAML.load(open(joinpath(welldir,fname)))
	observationwell = yamldata["Observation well"]
	observationbegintime = yamldata["Observation begin time"]
	endtime = yamldata["End time"]

	portdescr=[]
	try
		portdescr = string(yamldata["Screen number"])
	catch
		portdescr = "SINGLE COMPLETION"
	end

	baddates = []
	try
		baddates = yamldata["Bad dates"]
	catch
	end

	jumpfixdates = []
	jumpfixsize = []
	try
		jumpfixdates = map((x) -> DateTime(x[1],dateform), yamldata["Observation jumps"])
		jumpfixsize = map((x) -> x[2], yamldata["Observation jumps"])
	catch
	end

	samplethreshold=[]
	try
		samplethreshold = controldata["Sample threshold"]
	catch
		samplethreshold = 0.08
	end

	samplelength=[]
	try
		samplelength = controldata["Sample length"]
	catch
		samplelength = 5
	end

	barometricbegintime = Dates.format(DateTime(observationbegintime,dateform)+Dates.Day(-1),"yyyy-mm-dd")
	barometricendtime = Dates.format(DateTime(endtime,dateform)+Dates.Day(1),"yyyy-mm-dd")

	#------------------------------------------------------------------------------
	# Data processing
	#------------------------------------------------------------------------------
	db.connecttodb()

	# get the water level information
	waterlevels, waterleveltimes = db.getwaterlevels(observationwell, portdescr, observationbegintime, endtime)

	# remove the bad dates
	for baddate in baddates
		baddate_datetime = map((x) -> DateTime(x,dateform), hcat(baddate...))
		goodwaterlevels = []
		goodwaterleveltimes = []
		for j in 1:length(waterlevels)
		if waterleveltimes[j] > baddate_datetime[1] && waterleveltimes[j] < baddate_datetime[2]
		else
			push!(goodwaterlevels,waterlevels[j])
			push!(goodwaterleveltimes,waterleveltimes[j])
		end
	end
	waterlevels = goodwaterlevels
	waterleveltimes = goodwaterleveltimes
	end

	# fix the jumps in the water levels
	j = 1
	for jumpfixdate in jumpfixdates
		i = 1
		while i < length(waterleveltimes) && waterleveltimes[i] < jumpfixdate
			i += 1
		end
		while i < length(waterleveltimes)
			waterlevels[i] += jumpfixsize[j]
			i += 1
		end
	j += 1
	end

	# get the barometric information
	baropressmb, barotimes = db.getbarometricpressure(barometricbegintime, barometricendtime)

	# get the earth tide information -- for now, just read it from a file
	earthtideforces, earthtidetimes = db.getearthtide(barometricbegintime, barometricendtime)

	waterlevelsnosamp, waterleveltimesnosamp = cb.removeSamplingEvents(waterlevels, waterleveltimes, threshold=samplethreshold, samplinglength=samplelength)
	nosampwaterlevels, nosampwaterleveltimes = cb.denoise(waterlevelsnosamp, waterleveltimesnosamp, baropressmb, barotimes, earthtideforces, earthtidetimes)
	waterlevels = nosampwaterlevels
	waterleveltimes = nosampwaterleveltimes

	# remove the bad dates from the no samp water levels
	for baddate in baddates
		baddate_datetime = map((x) -> DateTime(x,dateform), hcat(baddate...))
		goodwaterlevels = []
		goodwaterleveltimes = []
		for j in 1:length(waterlevels)
			if waterleveltimes[j] > baddate_datetime[1] && waterleveltimes[j] < baddate_datetime[2]
			else
				push!(goodwaterlevels,waterlevels[j])
				push!(goodwaterleveltimes,waterleveltimes[j])
			end
		end
		waterlevels = goodwaterlevels
		waterleveltimes = goodwaterleveltimes
	end

	db.disconnectfromdb()

	# @show minimum(waterleveltimes)
	# @show maximum(waterleveltimes)
	return waterleveltimes,waterlevels
end

function dbPumpingRates(productionwells, begintime, endtime)
	# Get pumpingrates from mysql database using aquiferdb
	db.connecttodb()
	#get the production information
	production = Dict()
	for productionwell in productionwells
		i = find(productionwells .== productionwell)[1]
		production[productionwell] = Dict()
		production[productionwell]["rates"], production[productionwell]["times"] = db.getpumpingrates(productionwell, begintime, endtime)
		production[productionwell]["x"], production[productionwell]["y"], production[productionwell]["r"] = db.getgeometry(productionwell)
	end
	db.disconnectfromdb()
	return production
end # end module
