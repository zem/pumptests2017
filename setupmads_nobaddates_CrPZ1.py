import matplotlib
matplotlib.use("Agg")
import sys
import yaml
import datetime
import aquiferdb as db
import chipbeta as cb
#import dataproc as dp
import matplotlib.pyplot as plt
import wellspy
import os

if len(sys.argv) != 2 and len(sys.argv) != 3:
	print "Usage: python setupmads.py controlfile.yaml [makeplot]"
	sys.exit(1)

rootname = ".".join(sys.argv[1].split(".")[:-1])

if len(sys.argv) == 3 and sys.argv[2] == "makeplot":
	makeplot = True
else:
	makeplot = False

def datetimestring2datetime(datetimestr):
	datestr, timestr = datetimestr.split()
	datesplit = datestr.split("-")
	year, month, day = map(int, datesplit)
	timesplit = timestr.split(":")
	hour, minute, second = map(int, timesplit)
	return datetime.datetime(year, month, day, hour, minute, second)

def datetime2datetimestring(dt):
	return datetime2datestring(dt) + " " + ":".join(map(lambda i: str(i).zfill(2), [dt.hour, dt.minute, dt.second]))

def datestring2datetime(datestr):
	splitstr = datestr.split("-")
	return datetime.datetime(*map(int, splitstr))

def datetime2datestring(dt):
	return "-".join(map(lambda i: str(i).zfill(2), [dt.year, dt.month, dt.day]))

controlfilename = sys.argv[1]
with open(controlfilename, "r") as f:
	controldata = yaml.load(f)
	f.close()

productionwells = controldata["Production wells"]
observationwell = controldata["Observation well"]
observationbegintime = controldata["Observation begin time"]
try:
	#portdescr = "P" + str(controldata["Screen number"]) + "A"
	portdescr = str(controldata["Screen number"])
except KeyError:
	portdescr = "SINGLE COMPLETION"
try:
	pumpingbegintime = controldata["Pumping begin time"]
except KeyError:
	dt = datestring2datetime(observationbegintime)
	pumpingbegintime = datetime2datestring(dt - datetime.timedelta(days=365))
endtime = controldata["End time"]
barometricbegintime = datetime2datestring(datestring2datetime(observationbegintime) + datetime.timedelta(days=-1))
barometricendtime = datetime2datestring(datestring2datetime(endtime) + datetime.timedelta(days=1))
try:
	jumpfixdates = map(lambda x: datetimestring2datetime(x[0]), controldata["Observation jumps"])
	jumpfixsize = map(lambda x: x[1], controldata["Observation jumps"])
except KeyError:
	jumpfixdates = []
	jumpfixsize = []
try:
	samplethreshold = controldata["Sample threshold"]
except KeyError:
	samplethreshold = 0.08
try:
	samplelength = controldata["Sample length"]
except KeyError:
	samplelength = 5
try:
	allbaddates = map(lambda x: [datetimestring2datetime(x[0]), datetimestring2datetime(x[1])], controldata["Bad dates"])
except KeyError:
	allbaddates = []

# GOAL: shut off calibration of certain params to model zero drawdowns for certain wells.
# - Introduce "dummyprodwells" (YAML variable "Ignore production wells")
try:
	dummyprodwells = controldata["Ignore production wells"]
except KeyError:
	dummyprodwells = []

db.connecttodb()

#get the water level information
waterlevels, waterleveltimes = db.getwaterlevels(observationwell, portdescr, observationbegintime, endtime)
#remove the bad dates
for baddates in allbaddates:
	goodwaterlevels = []
	goodwaterleveltimes = []
	for i in range(len(waterlevels)):
		if not(baddates[0] < waterleveltimes[i] and waterleveltimes[i] < baddates[1]):
			goodwaterlevels.append(waterlevels[i])
			goodwaterleveltimes.append(waterleveltimes[i])
	waterlevels = goodwaterlevels
	waterleveltimes = goodwaterleveltimes
#fix the jumps in the water levels
j = 0
for jumpfixdate in jumpfixdates:
	i = 0
	while i < len(waterleveltimes) and waterleveltimes[i] < jumpfixdate:
		i += 1
	while i < len(waterleveltimes):
		waterlevels[i] += jumpfixsize[j]
		i += 1
	j += 1
	

#get the production information
production = {}
for productionwell in productionwells:
	production[productionwell] = {}
	production[productionwell]["rates"], production[productionwell]["times"] = db.getpumpingrates(productionwell, pumpingbegintime, endtime)
	production[productionwell]["x"], production[productionwell]["y"], production[productionwell]["r"] = db.getgeometry(productionwell)

#get the barometric information
baropressmb, barotimes = db.getbarometricpressure(barometricbegintime, barometricendtime)

#get the earth tide information -- for now, just read it from a file
earthtideforces, earthtidetimes = db.getearthtide(barometricbegintime, barometricendtime)

waterlevelsnosamp, waterleveltimesnosamp = cb.removeSamplingEvents(waterlevels, waterleveltimes, threshold=samplethreshold, samplinglength=samplelength)

#goodwaterlevels, goodwaterleveltimes = cb.denoise(waterlevels, waterleveltimes, baropressmb, barotimes, earthtideforces, earthtidetimes)
nosampwaterlevels, nosampwaterleveltimes = cb.denoise(waterlevelsnosamp, waterleveltimesnosamp, baropressmb, barotimes, earthtideforces, earthtidetimes)
#plt.plot(waterleveltimes, waterlevels, 'b.', alpha=.1)
#plt.plot(nosampwaterleveltimes, nosampwaterlevels, 'k.', alpha=.25)
#plt.show()
#print [len(waterlevels), len(waterlevelsnosamp), len(nosampwaterlevels)]
#nosampwaterlevels = dp.correctBimodalData(nosampwaterlevels)

#remove the bad dates from the no samp water levels
waterlevels = nosampwaterlevels
waterleveltimes = nosampwaterleveltimes
#for baddates in allbaddates:
#	goodwaterlevels = []
#	goodwaterleveltimes = []
#	for i in range(len(waterlevels)):
#		if not(baddates[0] < waterleveltimes[i] and waterleveltimes[i] < baddates[1]):
#			goodwaterlevels.append(waterlevels[i])
#			goodwaterleveltimes.append(waterleveltimes[i])
#	waterlevels = goodwaterlevels
#	waterleveltimes = goodwaterleveltimes
nosampwaterlevels = waterlevels
nosampwaterleveltimes = waterleveltimes

waterlevelinfo = {}
waterlevelinfo[observationwell] = {}
waterlevelinfo[observationwell]["times"] = nosampwaterleveltimes
waterlevelinfo[observationwell]["waterlevels"] = nosampwaterlevels
waterlevelinfo[observationwell]["x"], waterlevelinfo[observationwell]["y"], waterlevelinfo[observationwell]["r"] = db.getgeometry(observationwell)
wellspy.makewellsandmadsfiles(production, waterlevelinfo, rootname)

# GOAL: Manually set desired parameters for dummy production wells from opt to null:
# - use variable dummyprodwells
# - i.e., shut off calibration of certain params to model zero drawdowns for certain wells.
if portdescr != "SINGLE COMPLETION":
	madsfilename = observationwell + "#" + portdescr + ".mads"
else:
	madsfilename = observationwell + ".mads"

for dummyprodwell in dummyprodwells:
	oldstr = "- K_" + dummyprodwell + ": {init: 2.5, init_max: 4, init_min: -1, log: false, max: 8, min: -6, step: 0.1, type: opt}"
	newstr = "- K_" + dummyprodwell + ": {init: 6.0, init_max: 4, init_min: -1, log: false, max: 8, min: -6, step: 0.1, type: null}"
	os.system("find ./ -name " + madsfilename + " -exec sed -i \"s/" + oldstr + "/" + newstr + "/g\" {} \;")
	oldstr = "- S_" + dummyprodwell + ": {init: -1.8, init_max: -0.5, init_min: -4, log: false, max: 0, min: -6, step: 0.1, type: opt}"
	newstr = "- S_" + dummyprodwell + ": {init: -0.3, init_max: -0.5, init_min: -4, log: false, max: 0, min: -6, step: 0.1, type: null}"
	os.system("find ./ -name " + madsfilename + " -exec sed -i \"s/" + oldstr + "/" + newstr + "/g\" {} \;")

db.disconnectfromdb()

# The months vectors below determine the x axis tick spacing. Using monthsl triggers
# wellspy to set the minimum date to 2017. Currently plotting both.
monthsl = [1, 3, 5, 7, 9, 11]
monthss = [1, 5, 9]
monthsm = [1, 4, 7, 10]
if makeplot:
	if portdescr != "SINGLE COMPLETION":
		title = observationwell + " #" + str(portdescr)
	else:
		title = observationwell
	try:
		wellspy.plotspointfile(rootname + ".s_point", waterlevelinfo, title=title, months=controldata["Months"])
	except KeyError:
		wellspy.plotspointfile(rootname + ".s_point", waterlevelinfo, title=title, months=monthsm)
	#plt.show()
	plt.savefig(rootname + ".pdf")
	plt.savefig(rootname + ".png", dpi=300)
	try:
		wellspy.plotspointfile(rootname + ".s_point", waterlevelinfo, title=title, months=controldata["Months"])
	except KeyError:
		wellspy.plotspointfile(rootname + ".s_point", waterlevelinfo, title=title, months=monthsl)
	#plt.show()
	plt.savefig(rootname + "_2017.pdf")
	plt.savefig(rootname + "_2017.png", dpi=300)

"""
#plt.plot(goodwaterleveltimes, goodwaterlevels, '+')
plt.plot(waterleveltimes, waterlevels, 'b+')
plt.title(observationwell + " #" + str(portdescr))
plt.plot(nosampwaterleveltimes, nosampwaterlevels, 'k.')
#plt.plot(barotimes, baropressmb)
#plt.show()
plt.savefig("blah.pdf")
"""
