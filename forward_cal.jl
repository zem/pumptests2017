# Id: forward_cal.jl, Wed 04 Jan 2017 01:26:53 PM MST pandeys #
# Created by Sachin Pandey, LANL
# Description: Run calibrated forward model and plot results
#  - This script uses a modified version of setupmads.py (setupmads_nobaddates.py)
#        so that there are no time gaps in the model output
#  - Use: visualization of calibration results with continuous model curves
#------------------------------------------------------------------------------
import Mads

function wellsforward_nobaddates(yamlfile)
    # Initialize
    basedir = pwd()
    welldir =  split(yamlfile,"/")[1]
    fname = split(yamlfile,"/")[2]
    wellname = split(fname,".")[1]

    # Move appropriate yaml file to forward run directory
    if !isdir(joinpath(basedir,mydir,"forward"))
        mkdir(joinpath(basedir,mydir,"forward"))
    end

    cd(joinpath(basedir,mydir,"forward"))

    if !isdir(welldir)
        mkdir(welldir)
    end

    cd(joinpath(basedir,mydir,"forward",welldir))
    cp(joinpath("../..",yamlfile),fname,remove_destination=true)

    # Run setupmads_noBadDates
    cd(basedir)
    run(`python setupmads_nobaddates.py $(joinpath(mydir,"forward",yamlfile))`)

    # Replace initial conditions with calibrated values
    mdopt = Mads.loadmadsfile(joinpath(mydir,welldir,"$wellname-rerun-rerun.mads"))
    params = Mads.getparamsinit(mdopt)
    keys = Mads.getparamkeys(mdopt)
    mdbd = Mads.loadmadsfile(joinpath(mydir,"forward",welldir,"$wellname.mads")) # Use old parameters as search criteria
    params_init = Mads.getparamsinit(mdbd)

    cd(joinpath(basedir,mydir,"forward",welldir))
    # Replace floats
    for i in 1:length(keys)
        run(`find ./ -name $wellname.mads -exec sed -i "s/- $(keys[i]): {init: $(params_init[i]),/- $(keys[i]): {init: $(params[i]),/g" {} \;`)    
    end

    # Replace integers
    for i in 1:length(keys)
        run(`find ./ -name $wellname.mads -exec sed -i "s/- $(keys[i]): {init: $(Int(round(params_init[i]))),/- $(keys[i]): {init: $(params[i]),/g" {} \;`)    
    end

    # Run mads forward
    cd(basedir)
    run(`mads $(joinpath(mydir,"forward",welldir,"$wellname.mads")) forward`)

    # Make plot! (from main directory)
    run(`python plot_theis_driver.py $(joinpath(mydir,"forward",welldir,"$wellname.yaml"))`)

    cd(basedir)
end

if length(ARGS) < 1
    println("Arguments must be of the form mydir")
    println("e.g. julia forward_cal.jl chromium_2016")
    quit()
end

mydir = ARGS[1]
include(joinpath(mydir,"wellnames.jl"))
for yamlfile in yamlfiles
    try
        wellsforward_nobaddates(yamlfile)
        println("Success: $yamlfile")
    catch
    	println("Error: $yamlfile")
    end
end
