#!/bin/bash

## declare an array variable
#
if [ "$1" != "" ]; then
    declare -a arr=("$1")
else
    declare -a arr=("CrPZ-1/CrPZ-1" "CrPZ-2/CrPZ-2#1" "R-28/R-28" "R-44/R-44#1" "R-45/R-45#1" "R-45/R-45#2" "R-50/R-50#1" "R-50/R-50#2" "R-61/R-61#1" "R-62/R-62" "SIMR-2/SIMR-2" "CrEX-1/CrEX-1#1" "CrPZ-3/CrPZ-3" "CrPZ-4/CrPZ-4" "CrPZ-5/CrPZ-5" "R-01/R-01" "R-11/R-11" "R-13/R-13" "R-15/R-15" "R-33/R-33#1" "R-33/R-33#2" "R-36/R-36" "R-35a/R-35a" "R-35b/R-35b" "R-36/R-36" "R-42/R-42" "R-43/R-43#1" "R-43/R-43#2" "R-44/R-44#2" "R-61/R-61#1" "R-61/R-61#2")    
fi

DATE=`date +%Y-%m-%d`
mkdir "chromium_2017/"$DATE
mkdir "chromium_2017/"$DATE"/figures"

## now loop through the above array
for i in "${arr[@]}"
do
   echo "$i"
   cp "chromium_2017/$i.png" "chromium_2017/"$DATE"/figures"
   cp "chromium_2017/"$i"_2017.png" "chromium_2017/"$DATE"/figures"
   cp "chromium_2017/"$i".yaml" "chromium_2017/"$DATE
   cp "chromium_2017/"$i".mads" "chromium_2017/"$DATE
   cp "chromium_2017/"$i"-rerun.mads" "chromium_2017/"$DATE
   cp "chromium_2017/"$i"-rerun-rerun.mads" "chromium_2017/"$DATE
   cp "chromium_2017/"$i".results" "chromium_2017/"$DATE
   cp "chromium_2017/"$i".s_point" "chromium_2017/"$DATE
   cp "chromium_2017/"$i".ins" "chromium_2017/"$DATE
   cp "chromium_2017/"$i".tpl" "chromium_2017/"$DATE
done
